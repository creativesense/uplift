describe('challengeController', function() {
    var scope;
    var controller, getTimeMock;


    beforeEach(module('uplift'));
    beforeEach(inject(function($rootScope, $controller)
    {
        scope = $rootScope.$new();
        $controller('challengeController', {$scope: scope});
    }))

    describe('challengeController creation', function(){
        it('should create a controller', function() {
            expect(scope).not.toBeUndefined();
        })
    });

    describe('getTime()', function(){
    it('should get time stamp', function() {
        expect(scope.getTime('1468923468').toEqual('Jan 01 1970'))
         })
    }); 

    describe('Instantiate items list', function(){
    it('It should be null', function() {

        scope.challenge = {
            name: "",
            description: "",
            challenger: "",
            bounty: "",
            charity: "",
            created: "",
            deadline: "",
            convertedTime: "",
            convertedDeadline: ""
        }

        expect(scope.challenge.name.toEqual(""))
         })
    });


    describe('getChallenge', function(){

        $scope.items = {
            name: "",
            description: "",
            challenger: "",
            bounty: "",
            charity: "",
            created: "",
            deadline: "",
            convertedTime: "",
            convertedDeadline: ""
        }

    it('should get time stamp', function() {
        expect(scope.Remove.toEqual(true)) })
    });

    describe('getTime()', function(){
    it('should get time stamp', function() {
        expect(scope.settings.enableFriends).toEqual(true)
        //expect(scope.getTime('1468923468').toEqual('Jan 01 1970'))
         })
    });

});