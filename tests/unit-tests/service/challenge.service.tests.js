describe('Challenge Service Test', function (){
    var ChallengeService;

    beforeEach(module('uplift'));
    beforeEach(module('challengeService'));

    beforeEach(inject(function (_ChallengeService_) {
        ChallengeService = _ChallengeService_;
    }));
    

    describe('Initial challenge should be empty', inject(function(ChallengeService){
        it("Initial challenge", function() {
            expect(ChallengeService.challenge.name).toEqual("");
        })
    }));
})