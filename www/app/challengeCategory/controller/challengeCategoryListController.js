'use strict';
/**
 * Challenge Controller
 * Handles all the interactions in the challenge page.
 *
 * @package     challenge
 * @author      Martin Ombura <martin.omburajr@gmail.com>,
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 */

angular.module('uplift').controller('challengeCategoryListController', function($scope, $rootScope, $stateParams, $ionicHistory,challengeCategoryService) {  
    $scope.challengeCategories = challengeCategoryService.getAllChallengeCategories();
    $scope.myObj = {
        "color" : "white",
        "background-color" : "coral",
        "font-size" : "100px",
        "padding" : "50px"
    }
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
})