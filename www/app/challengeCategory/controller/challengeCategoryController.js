'use strict';
/**
 * Challenge Category List Controller
 * Handles all the interactions in the challenge page.
 *
 * @package     challengecategory
 * @author       Martin Ombura <martin.omburajr@gmail.com>,
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 */

angular.module('uplift').controller('challengeCategoryController', function($scope, $rootScope, $stateParams, $timeout, $ionicHistory, 
                                                                    $http, $cordovaCamera, challengeService, 
                                                                   challengeCategoryService, ionicMaterialMotion, ionicMaterialInk,
                                                                    $ionicPopup) {  

    $scope.challengeCategoryId = $stateParams.id;
    $scope.challengeCategory = challengeCategoryService.getChallengeCategoryById($scope.challengeCategoryId);
    console.log($scope.challengeCategories)
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";

    
})