'use strict';

/**
 * Friends Controller
 * Handles all the interactions in the friends page.
 *
 * @package     friends
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the friends page.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 */

angular.module('uplift').controller('friendRequestController', function ($scope, $rootScope, $stateParams, 
$timeout, $ionicHistory, ionicMaterialInk, ionicMaterialMotion, userService, friendRequestService, friendService, Users, $state, $location)
{
    // Set Header
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.loggedUserId = $rootScope.User.GetAuthData().uid;
    $scope.friendRequests = friendRequestService.getAllOpenFriendRequests($scope.loggedUserId);
    $scope.getUsername = function(userId){
        return userService.getUsernameById(userId);
    }

    $scope.getTime = function(time){
        return genericService.getTime(time);
    }

    //console.log($scope.friendRequests);
    //$rootScope.friendRequestCount= $scope.friendRequests.length
    $scope.$parent.showHeader();
    // $scope.$parent.clearFabs();
    // $scope.$parent.setHeaderFab('left');
    // // Delay expansion
    // $timeout(function() {
    //     $scope.isExpanded = true;
    //     //$scope.$parent.setexpanded(true);
    // }, 300);

    $scope.acceptFriendRequest = function(friendRequestId) {
        friendRequestService.acceptFriendRequest($scope.loggedUserId, friendRequestId);
    }

    $scope.declineFriendRequest = function(friendRequestId) {
        friendRequestService.declineFriendRequest($scope.loggedUserId, friendRequestId);
    }


    // Set Motion
    //ionicMaterialMotion.fadeSlideInRight();
    // Set Ink
    //ionicMaterialInk.displayEffect();
})