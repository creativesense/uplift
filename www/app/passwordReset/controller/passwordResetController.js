'use strict';

/**
 * PasswordReset Controller
 * Handles all the interactions in the passowrd reset page.
 *
 * @package     passwordReset
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope	Handles the functions and models applied in the page.
 * @param   Auth    Handles the authorisation of the users.                     
 */

angular.module('uplift').controller('passwordResetController', function ($scope, Auth, ionicMaterialMotion, ionicMaterialInk, $timeout){

    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    
    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });

    // Delay Slide In
    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

    // Set Ink
    ionicMaterialInk.displayEffect();
	
    $scope.user=
    {
      email:""
    }
    
    $scope.resetPassword = function(){
		var email = $scope.user.email;
		Auth.resetPassword(email);
	};

})