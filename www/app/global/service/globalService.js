'use strict';

/**
 * Challenge Service
 * Handles all the business logic a service requires.
 *
 * @package     challenge
 * @author      Martin Ombura <martin.omburajr@gmail.com>,
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   var              Handles the functions and models applied in the  page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   $ionicHistory       Keeps track of the views navigated by the user 
 * @param   $http               
 * @param   $firebaseArray      The entire data stored in firebase.
 * @param   $firebaseObject     
 * @param   $cordovaCamera      
 * @param   Challenges          List of all the challenges.          
 * @param   Users               List of all the users.
 * @param   Auth                Handles the authorisation of the users.                      
 * @param   Attachments         List of all the attachments.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 */

angular.module('uplift').service("globalService", function(Auth, Data, User, $firebaseArray, $firebaseObject, 
                                            $cordovaCamera, Challenges) {
    
    //Generate Random Id

    //Generates random id
    this.generateRandomId = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
         return s4() + s4() + '-' + s4() + '-' + s4() + '-' +  s4() + '-' + s4() + s4() + s4();
    }

        //Converts firebase time stamp to a unix  timestamp
    this.getTime = function(UNIX_timestamp){
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var time = date + ' ' + month + ' ' + year + ' | ' + hour + ':' + min ;
        return time;
    }

    
 })