'use strict';
angular.module('uplift').controller('vendorProfileController', function($scope, $stateParams, vendorService, $ionicHistory) {
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.vendorId = $stateParams.id;
    $scope.vendor = vendorService.getVendorById($scope.vendorId);
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');

})
   

