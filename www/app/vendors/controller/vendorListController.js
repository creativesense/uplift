'use strict';
angular.module('uplift').controller('vendorListController', function($scope, $stateParams, vendorService, $ionicHistory) {
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.vendors =  vendorService.getAllVendors();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');

    $scope.myObj = {
        "color" : "white",
        "font-size" : "30px",
        "padding" : "50px"
    }

})
   

