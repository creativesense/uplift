'use strict';
angular.module('uplift').controller('productListController', function($scope, $stateParams, vendorService, $ionicHistory) {
    $scope.vendorId = $stateParams.id;
    $scope.products =  vendorService.getProductsByVendor($scope.vendorId);
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');

    $scope.style = {
        "color" : "white",
        "background-color" : "coral",
        "font-size" : "100px",
        "padding" : "50px"
    }
})
   

