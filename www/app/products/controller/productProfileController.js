'use strict';
angular.module('uplift').controller('productProfileController', function($scope, $stateParams, vendorService, $ionicHistory) {
    $scope.productId = $stateParams.id;
    $scope.product = vendorService.getProductById($scope.productId);
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');

})
   

