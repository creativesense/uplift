'use strict';
/**
 * ExploreController
 * Allows for the search and adding of people.
 *
 * @package     Explore
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 *              
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 */

angular.module('uplift').controller('exploreController', function($scope, $rootScope, $stateParams, $timeout, $ionicHistory, 
                                                                    $http, $firebaseArray, $firebaseObject, $cordovaCamera, userService,
                                                                    Users, ionicMaterialMotion, ionicMaterialInk, friendRequestService, friendService,
                                                                    $ionicPopup) {  
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');
    
    $scope.loggedUserId = $rootScope.User.GetAuthData().uid;
    $scope.friends = friendService.getFriends($scope.loggedUserId);
    console.log($scope.friends)
    $scope.users = userService.getAllUsers();
    console.log($scope.users) 

    $scope.addUser = function(receiverId) {
        var val = friendRequestService.createFriendRequest($scope.loggedUserId, receiverId);
        $scope.showAlert(true, receiverId);
    }



    // $scope.exploreFriends = function(users) {
    //     for(var i = 0; i < users.length; i++) {
    //     var isFriend = friendService.isFriend($scope.loggedUserId, users[i].id)
    //         if(isFriend) {
    //             console.log("TRUE, THEY ARE FRIENDS!") 
    //              users[i].canAdd = false;              
    //         }
    //     }
    // }

    $scope.showAlert = function(bool, receiverId) {
        if(bool) {
        var alertPopup = $ionicPopup.alert({
            title: 'Friend Request',
            template: 'Succesfully Sent to ' + receiverId
        });
        } else {
        var alertPopup = $ionicPopup.alert({
            title: 'Friend Request',
            template: 'Upps.. We ran into an error. Try again later.'
        });
        }
    }

})