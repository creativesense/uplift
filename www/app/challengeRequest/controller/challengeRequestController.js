'use strict';

/**
 * Friends Controller
 * Handles all the interactions in the friends page.
 *
 * @package     friends
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the friends page.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 */

angular.module('uplift').controller('challengeRequestController', function ($scope, $rootScope, $stateParams, 
$timeout, ionicMaterialInk, ionicMaterialMotion, challengeRequestService, friendRequestService, friendService, Users, $state, $location, $ionicPopup)
{
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.loggedUserId = $rootScope.User.GetAuthData().uid;
    $scope.challengeRequests = challengeRequestService.getAllOpenChallengeRequests($scope.loggedUserId);
    $rootScope.challengeRequestCount= $scope.challengeRequests.length
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('left');
    // Delay expansion
    $timeout(function() {
        $scope.isExpanded = true;
        //$scope.$parent.setexpanded(true);
    }, 300);

    $scope.acceptChallengeRequest = function(challengeRequestId) {
        challengeRequestService.acceptChallengeRequest($scope.loggedUserId, challengeRequestId);
        $rootScope.alert("Challenge", "Challenge Request Accepted")
    }

     $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
        title: 'Challenge',
        template: 'The challenge request was accepted.'
    });

   alertPopup.then(function(res) {
       $rootScope.$broadcast("refreshChallenges");
       $state.go('app.profile');
       $scope.clean();
   });
 };
   
    $scope.declineChallengeRequest = function(challengeRequestId) {
        challengeRequestService.declineChallengeRequest($scope.loggedUserId, challengeRequestId);
        $rootScope.alert("Challenge", "Challenge Request Denied")
    }


    // Set Motion
    //ionicMaterialMotion.fadeSlideInRight();
    // Set Ink
    ionicMaterialInk.displayEffect();

    $scope.$on("refreshChallengeRequests", function () {
        $scope.init();
    });
})