'use strict';

/**
 * Friends Controller
 * Handles all the interactions in the friends page.
 *
 * @package     friends
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the friends page.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 */

angular.module('uplift').controller('previewChallengeController', function ($scope, $rootScope, $stateParams, 
$timeout, ionicMaterialInk, ionicMaterialMotion, challengeRequestService, friendRequestService, friendService, Users, $state, $location)
{
    // Set Header
    $scope.loggedUserId = $rootScope.User.GetAuthData().uid;
    $scope.challengeRequestId = $stateParams.id;
    $scope.previewChallenge = challengeRequestService.getChallengePreview($scope.challengeRequestId);
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('left');
    // Delay expansion
    $timeout(function() {
        $scope.isExpanded = true;
        $scope.$parent.setexpanded(true);
    }, 300);

    $scope.acceptChallengeRequest = function(challengeRequestId) {
        console.log(challengeRequestId);
        challengeRequestService.acceptChallengeRequest($scope.loggedUserId, challengeRequestId);
        $rootScope.alert("Challenge", "Challenge Request Accepted")
    }

    $scope.declineChallengeRequest = function(challengeRequestId) {
        challengeRequestService.declineChallengeRequest($scope.loggedUserId, challengeRequestId);
        $rootScope.alert("Challenge", "Challenge Request Denied")
    }

    // Set Motion
    //ionicMaterialMotion.fadeSlideInRight();
    // Set Ink
    ionicMaterialInk.displayEffect();
})