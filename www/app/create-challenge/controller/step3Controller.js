angular.module('uplift').controller('step3Controller', function($rootScope, $scope, Auth, ionicMaterialMotion, $ionicSideMenuDelegate, ionicMaterialInk, $timeout, friendService, charityService){

    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });

    // Delay Slide In
    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

    // Set Ink
    ionicMaterialInk.displayEffect();
	
    $scope.charities = charityService.getAllCharities();
    $scope.background = $rootScope.category.image;
    $scope.next = function(){
        $rootScope.charity = this.charity;
	}

});