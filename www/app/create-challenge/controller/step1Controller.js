angular.module('uplift').controller('step1Controller', function($rootScope, $scope, $stateParams, Auth, ionicMaterialMotion, $ionicSideMenuDelegate, ionicMaterialInk, $timeout){

    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });
	
    $scope.background = $rootScope.category.image;

    $scope.next = function(){
        $rootScope.name = this.challenge.name;
        $rootScope.description = this.challenge.description;
	}

});