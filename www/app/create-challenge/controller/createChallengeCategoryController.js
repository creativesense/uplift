angular.module('uplift').controller('createChallengeCategoryController', function($scope, $rootScope, $stateParams, $ionicHistory 
                                                                    ,challengeCategoryService, userService
                                                                    ) {

    $scope.challengeCategories = challengeCategoryService.getAllChallengeCategories();
    $scope.$parent.clearFabs();
    $scope.myObj = {
        "color" : "white",
        "background-color" : "coral",
        "font-size" : "100px",
        "padding" : "50px"
    }
    $scope.next = function(){
        $rootScope.category = this.challengeCategory;
	}
});
