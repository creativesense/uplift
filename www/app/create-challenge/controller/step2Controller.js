angular.module('uplift').controller('step2Controller', function($rootScope, $scope, Auth, ionicMaterialMotion, $ionicSideMenuDelegate, ionicMaterialInk, $timeout, friendService, Users){

    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });

    // Delay Slide In
    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

    // Set Ink
    ionicMaterialInk.displayEffect();
	
    var loggedUser = $rootScope.User.GetAuthData().uid

    //$scope.friends = friendService.getFriends(loggedUser);
    $scope.users = Users.getAll();

    $scope.background = $rootScope.category.image;
    // $scope.friends = $scope.loggedUser.friends;

    $scope.next = function(){
        $rootScope.challengee = this.challengee.id;
	}

});