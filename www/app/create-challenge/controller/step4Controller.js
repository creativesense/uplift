angular.module('uplift').controller('step4Controller', function($rootScope, $scope, Auth, ionicMaterialMotion, $ionicSideMenuDelegate, ionicMaterialInk, $timeout, friendService, charityService){

    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });

    // Delay Slide In
    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

    // Set Ink
    ionicMaterialInk.displayEffect();

    $scope.slider = {
        value: 10,
        options: {
        floor: 0,
        ceil: 100,
        step: 5,
        minLimit: 10,
        maxLimit: 90
        }
    };

    $scope.charities = charityService.getAllCharities();
    $scope.background = $rootScope.category.image;
    $scope.next = function(){
        $rootScope.bounty = this.challenge.bounty;
        $rootScope.charityPercentage = this.challenge.charityPercentage;
	}

});