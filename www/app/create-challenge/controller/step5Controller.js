angular.module('uplift').controller('step5Controller', function($rootScope, $scope, $state, Auth, ionicMaterialMotion, $ionicPopup, $ionicSideMenuDelegate, ionicMaterialInk, $timeout, friendService, charityService, challengeRequestService){

    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });

    // Delay Slide In
    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

    $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
        title: 'Challenge',
        template: 'The challenge request was sent sucessfully.'
    });

   alertPopup.then(function(res) {
       $rootScope.$broadcast("refreshChallenges");
       $state.go('app.profile');
       $scope.clean();
   });
 };

    ionicMaterialInk.displayEffect();
    $scope.background = $rootScope.category.image;
    var tempChallenge = {};

    $scope.next = function(){
        tempChallenge.challenger = $rootScope.User.GetAuthData().uid;
        tempChallenge.challengee = $rootScope.challengee;
        tempChallenge.name = $rootScope.name;
        tempChallenge.category = $rootScope.category.name;
        tempChallenge.description = $rootScope.description;
        tempChallenge.bounty = $rootScope.bounty;
        tempChallenge.charityPercentage = $rootScope.charityPercentage
        tempChallenge.charity = $rootScope.charity.$id;
        tempChallenge.deadline = this.challenge.deadline.getTime();
        challengeRequestService.createChallengeRequest(tempChallenge.challenger, tempChallenge.challengee, tempChallenge);
        $scope.showAlert();
	}

    $scope.clean = function(){
        $rootScope.challengee = "";
        $rootScope.name ="";
        $rootScope.category = {};
        $rootScope.description = "";
        $rootScope.bounty = 0;
        $rootScope.charityPercentage= 0;
        $rootScope.charity = {};
    }
});
