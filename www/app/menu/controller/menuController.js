'use strict';

/**
 * Menu Controller
 * Handles all the interactions in the menu/common page.
 *
 * @package     menu
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 *
 *
 * @param   $scope          Handles the functions and models applied in the activity page.
 * @param   $rootScope      Handles the functions and models applied in the application.
 * @param   $timeout        Function that performs an operation after a giving time.
 * @param   $ionicModal     Enables the page to create content panes that go over the main view temporarily.
 * @param   User            Handles the interactions with the user.
 * @param   Auth            Handles the authorisation of the users.
 */

angular.module('uplift').controller('menuController', function($scope, $rootScope, $ionicModal, $timeout, User, Auth) {

    // Form data for the login modal
    // $scope.isExpanded = false;
    // $scope.hasHeaderFabLeft = false;
    // $scope.hasHeaderFabRight = false;

    $scope.friendRequestCount = $rootScope.friendRequestCount;
    $scope.challengesCount = $rootScope.challengeCount;

    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
        });
    }

    /* Layout Methods */

    $scope.hideNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
    };

    $scope.showNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
    };

    $scope.noHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.setExpanded = function(bool) {
        $scope.isExpanded = bool;
    };

    $scope.setHeaderFab = function(location) {
        var hasHeaderFabLeft = false;
        var hasHeaderFabRight = false;

        switch (location) {
            case 'left':
                hasHeaderFabLeft = true;
                break;
            case 'right':
                hasHeaderFabRight = true;
                break;
        }

        $scope.hasHeaderFabLeft = hasHeaderFabLeft;
        $scope.hasHeaderFabRight = hasHeaderFabRight;
    };

    $scope.hasHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }

    };

    //Hide Header
    $scope.hideHeader = function() {
        $scope.hideNavBar();
        $scope.noHeader();
    };

    // Show Header
    $scope.showHeader = function() {
        $scope.showNavBar();
        $scope.hasHeader();
    };

    //Clear Fabs
    $scope.clearFabs = function() {
        var fabs = document.getElementsByClassName('button-fab');
        if (fabs.length && fabs.length > 1) {
            fabs[0].remove();
        }
    };

    // Logout
    $scope.logout = function(){
        $rootScope.showLoading("We hope to see you soon...");
        Auth.logout();
        $rootScope.hideLoading();
    }
})
