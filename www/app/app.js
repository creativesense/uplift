/**
 * Main Aplication File
 * Initiates database connection and runs the application.
 *
 * @package     app
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 */

angular.module('uplift', ['ionic', 'uplift.data', 'auth', 'user', 'firebase', 'ionic-material', 'ionMdInput', 'ngCordova', 'firebaseUserService', 
                                                'friendService', 'activityService', 'challengeCategoryService', 'friendRequestService', 'charityService', 'commentService', 'followerService',
                                                'challengeService', 'notificationService', 'attachmentService', 'challengeRequestService', 'genericService', 'rewardService', 'vendorService'])

// Firebase URL
.constant('FBURL', 'https://upliftsa.firebaseio.com/')

// Constructor injection for a Firebase reference
.service('FDBRoot', ['FBURL', Firebase])
//.service('FDBRoot', ['FBURL'])

// Run Function
    .run(function ($ionicPlatform, $rootScope, $state, Auth, User, $firebase, $timeout, $ionicLoading, FDBRoot, userService, $ionicPopup, $firebaseObject) {
    $rootScope.showLoading = function(msg) {
        $ionicLoading.show({
            //template: '<ion-spinner icon="bubbles" class="spinner-calm"></ion-spinner><br/>' + msg
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div><br>' + msg
        });
    };

    $rootScope.background = "img/" + Math.floor((Math.random() * 6) + 1) + ".jpg";

    $rootScope.hideLoading = function() {
        $ionicLoading.hide();
    };

    $rootScope.showLoading("Loading the app...");

    $ionicPlatform.ready(function() {
        $rootScope.User = User.get();
        

        $rootScope.err = '';

        $rootScope.DEBUG = true;
        $rootScope.clog = function(msg){
            if ($rootScope.DEBUG){
                console.log(msg);
            }
        };

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }



        $rootScope.checkSession = function(resetView) {
            $rootScope.clog("doso sam tu s: " + resetView);

            if($rootScope.User && $rootScope.User.GetAuthData()) {
                $state.go('app.profile');
            } 
            // else if($rootScope.User && $rootScope.User.GetAuthData() && !username){
            //     $state.go('app.username');
            // }
            else if(resetView) {
                $timeout(function(){
                    $state.go('app.login', {}, {reload: false});
                });
            }

            $rootScope.hideLoading();
        };  

        $rootScope.checkSessionWUsername = function(resetView, username) {
            $rootScope.clog("doso sam tu s: " + resetView);

            if($rootScope.User && $rootScope.User.GetAuthData() && username) {
                $state.go('app.profile');
            } 
            else if($rootScope.User && $rootScope.User.GetAuthData() && !username){
                $state.go('app.username');
            }
            else if(resetView) {
                $timeout(function(){
                    $state.go('app.login', {}, {reload: false});
                });
            }

            $rootScope.hideLoading();
        }; 

        $rootScope.alert = function (errTitle, errMsg){
            $ionicPopup.alert({
                title: errTitle,
                template: errMsg
            });
        };

        Auth.onAuth(function(authData) {
            $rootScope.hideLoading();

            if (! authData) {
                $rootScope.User && $rootScope.User.Reset();

                $rootScope.clog("Not logged in yet");

                $rootScope.checkSession(true);
            } else {
                $rootScope.userAuth = authData;

                $rootScope.User.SetUserAuthData(
                    authData, 
                    function(status) {
                        if(!status.ok) {
                            $rootScope.User.Reset();
                        }  
                        var userId = authData.uid;                
                        var usernameRef = FDBRoot.child("user").child(userId)
                        usernameRef.on("value", function(snapshot){
                            var usernameExist = snapshot.val().username
                            if(usernameExist == undefined){
                                usernameExist = null;
                            }
                            $rootScope.checkSessionWUsername(true, usernameExist); 
                        })     
                                      
                    }
                );
            }
        });

        // State that the application should go when it runs
        $state.go('app.login');
    });
})

// Configuration of the application
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Turn off caching for demo simplicity's sake
    //$ionicConfigProvider.views.maxCache(0);

    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    
    $ionicConfigProvider.scrolling.jsScrolling(false);
    /* States: The different pages of the application */
    
    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/menu/view/menu.html',
        controller: 'menuController'
    })

    
     /**
      * Charity List State
      */
    .state('app.username', {
        url: '/username',
        views: {
            'menuContent': {
                templateUrl: 'app/signup/view/username.html',
                controller: 'usernameController'
            },        
        }
    })


     /**
      * Charity List State
      */
    .state('app.addFollower', {
        url: '/addFollower/:challengeId',
        views: {
            'menuContent': {
                templateUrl: 'app/follower/view/addFollower.html',
                controller: 'followerController'
            },        
        }
    })

         /**
      * Charity List State
      */
    .state('app.challengeFollowerList', {
        url: '/challengeFollowerList',
        views: {
            'menuContent': {
                templateUrl: 'app/follower/view/challengeFollowerList.html',
                controller: 'followerController'
            },        
        }
    })

    /**
      * Charity List State
      */
    .state('app.charities', {
        url: '/charities',
        views: {
            'menuContent': {
                templateUrl: 'app/charities/view/charityList.html',
                controller: 'charityListController'
            },        
        }
    })

     /**
      * Charity Profile State
      */
    .state('app.charity', {
        url: '/charity/:id',
        views: {
        'menuContent': {
            templateUrl: 'app/charities/view/charityProfile.html',
            controller: 'charityProfileController'
            },
        }         
    })

     /**
      * Vendor List State
      */
    .state('app.vendors', {
        url: '/vendors',
        views: {
            'menuContent': {
                templateUrl: 'app/vendors/view/vendorList.html',
                controller: 'vendorListController'
            },        
        }
    })

     /**
      * Vendor Profile State
      */
    .state('app.vendor', {
        url: '/vendor/:id',
        views: {
        'menuContent': {
            templateUrl: 'app/vendors/view/vendorProfile.html',
            controller: 'vendorProfileController'
            },
        }         
    })

     /**
      * Challenge State
      */
    .state('app.challenge', {
        url: '/challenge/:id',
        templateUrl: 'app/challenge/view/challenge.html',
        views: {
        'menuContent': {
            templateUrl: 'app/challenge/view/challenge.html',
            controller: 'challengeController'
            },   
        // 'challengeTopBar@app.challenge': {
        //     templateUrl: 'app/challenge/view/challengeTopBar.html',
        //     controller: 'challengeController'
        //     },            
        'challengeUpper@app.challenge': {
            templateUrl: 'app/challenge/view/challengeUpper.html',
            controller: 'challengeController'
            },
        'challengeLower@app.challenge': {
            templateUrl: 'app/challenge/view/challengeLower.html',
            controller: 'challengeController'
            },
        'challengeDetails@app.challenge': {
            templateUrl: 'app/challenge/view/challengeDetails.html',
            controller: 'challengeController'
            },
        'challengeCommentList@app.challenge': {
            templateUrl: 'app/challenge/view/challengeCommentList.html',
            controller: 'challengeCommentController'
            },
        'challengeMedia@app.challenge': {
            templateUrl: 'app/challenge/view/challengeMedia.html',
            controller: 'challengeMediaController'
            },
        'challengeFollowerList@app.challenge': {
            templateUrl: 'app/challenge/view/challengeFollowerList.html',
            controller: 'challengeFollowerListController'
            },
        'challengeCreateComment@app.challenge': {
            templateUrl: 'app/challenge/view/challengeCreateComment.html',
            controller: 'challengeCommentController'
            }
        }
    })

    .state('app.challenge.followers', {
        url: '/challenge/:id',
        templateUrl: 'app/challenge/view/challengeFollowerList.html',
        controller: 'challengeFollowerListController'
    })

    .state('app.challenge.comments', {
        url: '/challenge/:id',
        templateUrl: 'app/challenge/view/challengeCommentList.html',
        controller: 'challengeCommentController'
    })    

    .state('app.challenge.media', {
        url: '/challenge/:id',
        templateUrl: 'app/challenge/view/challengeMedia.html',
        controller: 'challengeMediaController'
    })

        /**
     * Challenge category State
     *  */
    .state('app.challengeSummary', {
        url: '/challengeSummary/:id',
        views: {
        'menuContent': {
            templateUrl: 'app/challenge/view/challengeSummary.html',
            controller: 'challengeCompleteController'
            },
        }         
    })

    /**
     * Challenge category State
     *  */
    .state('app.challengeReward', {
        url: '/challengeReward/:id',
        views: {
        'menuContent': {
            templateUrl: 'app/challenge/view/challengeReward.html',
            controller: 'challengeCompleteController'
            },
        }         
    })

    /**
     * Challenge category State
     *  */
    .state('app.challengeCategory', {
        url: '/challengeCategory/:id',
        views: {
        'menuContent': {
            templateUrl: 'app/challengeCategory/view/challengeCategory.html',
            controller: 'challengeCategoryController'
            },
        }         
    })

    /**
     * Challenge category list State
     * */
    .state('app.challengeCategoryList', {
        url: '/challengeCategoryList',
        views: {
        'menuContent': {
            templateUrl: 'app/challengeCategory/view/challengeCategoryList.html',
            controller: 'challengeCategoryListController'
            },
        }         
    })

    /**
     * Login state
     *  */
    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'app/login/view/login.html',
                controller: 'loginController'
            },
            'fabContent': {
                template: ''
            }
        }
    })

    /**
     * Friend Requests State
     * */
    .state('app.friendRequests', {
        url: '/friendRequests',
        views: {
            'menuContent': {
                templateUrl: 'app/friendRequest/view/friendRequestList.html',
                controller: 'friendRequestController'
            }
        },
    })

    
    /**
     * Challenge Requests list State
     * */
    .state('app.challengeRequests', {
        url: '/challengeRequests',
        views: {
            'menuContent': {
                templateUrl: 'app/challengeRequest/view/challengeRequestList.html',
                controller: 'challengeRequestController'
            }
        },
    })

    /**
     * Challenge request preview State
     * */
    .state('app.challengeRequestPreview', {
        url: '/challengeRequests/preview/:id',
        views: {
            'menuContent': {
                templateUrl: 'app/challengeRequest/view/previewChallenge.html',
                controller: 'previewChallengeController'
            }
        },
    })


    
    /**
     * Profile State
     * */
    .state('app.profile', {
        url: '/profile',
        views: {
                    'menuContent': {
                        templateUrl: 'app/profile/view/profile.html',
                        controller: 'profileController'
                    },
                    'profileBanner@app.profile': {
                        templateUrl: 'app/profile/view/profileBanner.html',
                        controller: 'profileController'
                    },
                    'profileOptionsMenu@app.profile': {
                        templateUrl: 'app/profile/view/profileOptionsMenu.html',
                        controller: 'profileController'
                    },
                    'profileChallengeList@app.profile': {
                        templateUrl: 'app/profile/view/profileChallengeList.html',
                        controller: 'profileChallengeListController'
                    },
                    'fabContent': {
                        templateUrl: 'app/profile/view/profileAddButton.html',
                        controller: 'profileAddButtonController'               
                    }
            }
    })

    //     // Profile State
    // .state('app.profile', {
    //     url: '/profile',
    //     templateUrl: 'app/profile/view/profileAddButton.html',
    //     })
    // .state('app.profile.profileBanner', {
    //     url: '/profileBanner',
    //     templateUrl : 'app/profile/view/profile.html',
    //     controller: 'profileController'
    //     })
    // .state('app.profile.challengeList', {
    //     url: '/challengeList',
    //     templateUrl: 'app/profile/view/profileChallengeList.html',
    //     controller: 'profileController'
    //     })
          
    
     // Charity Profile State
    .state('app.comments', {
        url: '/comments',
        views: {
            'menuContent': {
                templateUrl: 'app/comments/view/comments.html',
                controller: 'commentsController', 
            },            
        },
        params: {
            challengeId: null
        }
    })

    // Friend Profile State
     .state('app.friendProfile', {
            url: '/friend/:friendId',
            views: {
                'menuContent': {
                    templateUrl: 'app/friends/view/friendsProfile.html',
                    controller: 'friendsProfileController',
                }
            }
        })

            // Friend Profile State
     .state('app.friendProfilePreview', {
            url: '/friend/:friendId',
            views: {
                'menuContent': {
                    templateUrl: 'app/friends/view/friendProfilePreview.html',
                    controller: 'friendsProfileController',
                }
            }
        })
    // Activity State
    .state('app.activity', {
        url: '/activity',
        views: {
            'menuContent': {
                templateUrl: 'app/activity/view/activity.html',
                controller: 'activityController'
            }
            // 'fabContent': {
            //     template: '<button id="fab-activity" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-paper-airplane"></i></button>',
            //     controller: function ($timeout) {
            //         $timeout(function () {
            //             document.getElementById('fab-activity').classList.toggle('on');
            //         }, 100);
            //     }
            // }
        }
    })

    // Friends State
    .state('app.friends', {
        url: '/friends',
        views: {
            'menuContent': {
                templateUrl: 'app/friends/view/friendsList.html',
                controller: 'friendsListController',
            }
        }
    })

    .state('challenge-tab', {
        url: '/challenge-tab',
        abstract: true,
        templateUrl: 'app/challenge/view/challenge-tab.html'
    })

    .state('app.explore1', {
        url: '/friends',
        views: {
            'menuContent': {
                templateUrl: 'app/explore/view/exploreList.html',
                controller: 'exploreController',
            }
        }
    })

    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'app/explore/view/explore-tab.html'
    })

    .state('tab.explore', {
        url: '/tab/explore',
        views: {
            'tab-explore': {
                templateUrl: 'app/explore/view/exploreList.html',
                controller: 'exploreController'
            }
        } 
    })

    .state('tab.friends', {
        url: '/tab/friends',
        views: {
            'tab-friends': {
                templateUrl: 'app/explore/view/exploreFriendsList.html',
                controller: 'exploreController',          
            }        
        } 
    })

        /**
     * Share State
     * */
    .state('app.share', {
        url: "/share",
        views: {
        'menuContent': {
            templateUrl: "app/share/view/share.html",
            controller: 'shareController'
        }
        }
    })
    /**
     * Email login State
     * */
    .state('app.emailLogin', {
        url: '/emailLogin',
        views: {
            'menuContent': {
                templateUrl: 'app/emailLogin/view/emailLogin.html',
                controller: 'emailLoginController'
            },
            'fabContent': {
                template: ''
            }
        }
    })

    /**
     * Signup State
     * */
    .state('app.signup', {
        url: '/signup',
        views: {
            'menuContent': {
                templateUrl: 'app/signup/view/signup.html',
                controller: 'signupController'
            },
          'fabContent': {
                template: ''
            }
        }
    })

    /**
     * Password Reset State
     * */
    .state('app.passwordReset', {
        url: '/passwordReset',
        views: {
            'menuContent': {
                templateUrl: 'app/passwordReset/view/passwordReset.html',
                controller: 'passwordResetController'
            },
            'fabContent': {
                template: ''
            }
        }
    })

    .state('app.create-challenge-category', {
       url: '/create-challenge-category',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/create-challenge-category.html',
                controller: 'createChallengeCategoryController',
            },
        }
    })
    
    .state('app.create-challenge', {
        url: '/create-challenge',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/create-challenge.html',
                controller: 'createChallengeController',
            },
        }
    })

    .state('app.step1', {
        url: '/step1',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/step1.html',
                controller: 'step1Controller',
            },
        }
    })

    .state('app.step2', {
        url: '/step2',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/step2.html',
                controller: 'step2Controller',
            },
        }
    })
    
    .state('app.step3', {
        url: '/step3',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/step3.html',
                controller: 'step3Controller',
            },
        }
    })

    .state('app.step4', {
        url: '/step4',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/step4.html',
                controller: 'step4Controller',
            },
        }
    })

    .state('app.step5', {
        url: '/step5',
        views: {
            'menuContent': {
                templateUrl: 'app/create-challenge/view/step5.html',
                controller: 'step5Controller',
            },
        }
    })
    //.get('*', routes.index)
    // If none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login/view/login.html');
})


/**
* This function filters and orders the list of objects that it receives.
*
* @param    items   The list of objects to be ordered
* @param    field   The field that the list should be sorted by
* @param    reverse If the list should be sorted or not the list should be sorted
*/
.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    for(var itemKey in items) {
        if(items.hasOwnProperty(itemKey) && items[itemKey] && items[itemKey].hasOwnProperty(field)) {
            filtered.push(items[itemKey]);
        }
    }
    if(filtered.length > 0) {
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
    }
    return filtered;
  };
})
;