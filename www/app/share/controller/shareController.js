'use strict';

angular.module('uplift').controller('shareController',['$scope',function($scope, $rootScope,$stateParams, $timeout) {

   $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setHeaderFab(false);

   $scope.whatsappShare=function(){
    window.plugins.socialsharing.shareViaWhatsApp('UPLIFT SA - For a better South Africa', null, "http://upliftsa.co.za" /* url */, null, function(errormsg){alert("Ups... Try again later.")});
  }
   $scope.twitterShare=function(){
    window.plugins.socialsharing.shareViaTwitter('UPLIFT SA - For a better South Africa', null, 'http://upliftsa.co.za', null, function(errormsg){alert("Ups... Try again later.")});
  }
  $scope.facebookShare=function(){
    window.plugins.socialsharing.shareViaFacebook('UPLIFT SA - For a better South Africa', null, 'http://upliftsa.co.za', null, function(errormsg){alert("Ups... Try again later.")});
  }
  $scope.instagramShare=function(){
   window.plugins.socialsharing.shareViaInstagram('UPLIFT SA - For a better South Africa', null, function() {console.log('share ok')}, function(errormsg){alert(errormsg)});
  } 
   $scope.OtherShare=function(){
     window.plugins.socialsharing.share('UPLIFT SA - For a better South Africa', null, '/icon.png', 'http://upliftsa.co.za');
  }
 
}])