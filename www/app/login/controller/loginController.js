'use strict';

/**
 * Login Controller
 * Handles all the interactions in the login page.
 *
 * @package     login
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope                  Handles the functions and models applied in the activity page.
 * @param   $rootScope              Handles the functions and models applied in the application.
 * @param   $timeout                Function that performs an operation after a giving time.
 * @param   $stateParams            Passes in the navigated url.
 * @param   Auth                    Handles the authorisation of the users.   
 * @param   ionicMaterialInk        Ink from Ionic-Material template.       
 * @param   $ionicLoading           Allows the page to display the loading circle.
 * @param   $ionicPopup             Allows the page to display popups.            
 * @param   $ionicSideMenuDelegate  Controls all the side menus.
 */

angular.module('uplift').controller('loginController', function($scope, $rootScope, $timeout, $state,
                                                          $stateParams, Auth, ionicMaterialInk, 
                                                          $ionicLoading, $ionicPopup, $ionicSideMenuDelegate, ionicMaterialMotion) {

    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    // Set Motion
    $timeout(function() {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });
    }, 300);
    
        // Delay Slide In
    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);
    
    $scope.user = {
        email: "",
        password: ""
    };
    
    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });

    $scope.$parent.clearFabs();

    // Hide Header
    $timeout(function() {
        $scope.$parent.hideHeader();
    }, 0);


    // Set Ink
    ionicMaterialInk.displayEffect();
    // Validate the user with email
    $scope.validateUser = function() {
        $rootScope.showLoading('Logging you in...');
        
        Auth.loginWithEmail(this.user.email, this.user.password)
        .then(function(user) {
                //do nothing and wait for onAuth
                console.log("logged in");
                $state.go("app.profile"); 
            }, function(error) {
                var errMsg = '';

                if (error.code == 'INVALID_EMAIL' || error.code == 'INVALID_PASSWORD') {
                    errMsg = 'Invalid Email/Password combination';
                }
                else if (error.code == 'INVALID_USER') {
                    errMsg = 'Invalid User';
                }
                else {
                    errMsg = 'Oops something went wrong. Please enter a valid email';
                }

                $rootScope.hideLoading();
                $rootScope.alert('<b><i class="icon ion-person"></i> Login error</b>', errMsg);
            });
    }

    //Validate user with external APIs
    $scope.oauthLogin = function(provider) {
        if(provider=='facebook') {
            Auth.loginWithFacebook()
                .then(function(authData) {
                    $rootScope.clog('oauthLogin');
                })
                .catch(function(error) {
                    $rootScope.clog('Oops something went wrong. Please try again,');
                    console.dir(error);
                });
        }
        else if (provider=='google') {
            Auth.loginWithGoogle()
                .then(function(authData) {
                    $rootScope.clog('oauthLogin');                 
                })
                .catch(function(error) {
                    $rootScope.clog('Oops something went wrong. Please try again.');
                    $rootScope.clog(error);
                });
        }
        else if (provider=='twitter') {
            Auth.loginWithTwitter()
                .then(function(authData) {
                    $rootScope.clog('oauthLogin');
                })
                .catch(function(error) {
                    $rootScope.clog('Oops something went wrong. Please try again.');
                    $rootScope.clog(error);
                });
        }
        else {
            $rootScope.hideLoading();
        }
    };
})