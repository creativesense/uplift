'use strict';
angular.module('uplift').controller('challengeCompleteController', function($scope, $rootScope, $stateParams, $timeout, $firebaseObject, challengeService, Users, ionicMaterialInk, ionicMaterialMotion, $ionicPopup, 
    $http, Auth, commentService, friendService, followerService, $ionicHistory, $cordovaCamera, $ionicModal) {
    // Set Header
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.challengeId = $stateParams.id;
    $scope.challenge = challengeService.getChallengeById($scope.challengeId);

    $scope.challengeSuccess = $scope.challenge.successfulCompletion
    $scope.charityEarnings = challengeService.calculateCharityEarnings($scope.challenge.bounty, $scope.challenge.charityPercentage);
    $scope.challengeeEarnings = challengeService.calculateChallengeeEarnings($scope.challenge.bounty, $scope.challenge.charityPercentage)
    $scope.adminEarnings = challengeService.calculateAdminEarnings($scope.challenge.bounty, $scope.challenge.charityPercentage)

    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');


    if($scope.challengeSuccess){
        //open success
    }else{
        //open failure modal
    }

    // $ionicModal.fromTemplateUrl('app/challenge/challengeCompleteSuccess.html', {
    // scope: $scope,
    // animation: 'slide-in-up',
    //     }).then(function(modal) {
    //         $scope.modal = modal;
    //     });
            
    //     $scope.openModal = function() {
    //         $scope.modal.show();
    //     };
            
    //     $scope.closeModal = function() {
    //         $scope.modal.hide();
    //     };
            
    //     //Cleanup the modal when we're done with it!
    //     $scope.$on('$destroy', function() {
    //         $scope.modal.remove();
    //     });
            
    //     // Execute action on hide modal
    //     $scope.$on('modal.hidden', function() {
    //         // Execute action
    //     });
            
    //     // Execute action on remove modal
    //     $scope.$on('modal.removed', function() {
    //         // Execute action
    //     });
    // })

    //  $ionicModal.fromTemplateUrl('app/challenge/challengeCompleteFailure.html', {
    // scope: $scope,
    // animation: 'slide-in-up',
    //     }).then(function(modal) {
    //         $scope.modal = modal;
    //     });
            
    //     $scope.openModal = function() {
    //         $scope.modal.show();
    //     };
            
    //     $scope.closeModal = function() {
    //         $scope.modal.hide();
    //     };
            
    //     //Cleanup the modal when we're done with it!
    //     $scope.$on('$destroy', function() {
    //         $scope.modal.remove();
    //     });
            
    //     // Execute action on hide modal
    //     $scope.$on('modal.hidden', function() {
    //         // Execute action
    //     });
            
    //     // Execute action on remove modal
    //     $scope.$on('modal.removed', function() {
    //         // Execute action
    //     });
    })

   

