'use strict';

/**
 * Profile Controller
 * Handles all the interactions in the profile page.
 *
 * @package     profile
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 *
 *
 * @param   $scope              Handles the functions and models applied in the page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   challengeService    List of all the challenges.
 * @param   Users               List of all the users.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 * @param   $ionicModal         Enables the page to create content panes that go over the main view temporarily.
 */

angular.module('uplift').controller('challengeHomeController', function ($scope, $rootScope, $stateParams, $timeout,
                                                                   Users, challengeService, friendService, 
                                                                   allChallengesRelatedToMeFirebase, 
                                                                   ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicModal) {
    $scope.myCreatedChallengesForFriends = allChallengesRelatedToMeFirebase.createdByMe;
    $scope.friendsCreatedChallengesForMe = allChallengesRelatedToMeFirebase.createdByFriends;
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);
    $scope.userForRemoveFromChallenge = null;
    $scope.profileImage = {
        'background-image': 'url(' + $scope.MetaData.profileImageURL + ')'
    };

    // Set Motion
    $timeout(function () {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });
    }, 300);

    // Set Ink
    //ionicMaterialInk.displayEffect();

    // Load user challenges and initialise page
    $scope.init = function () {
        $rootScope.showLoading("Loading data...");
        challengeService.getChallengeByUser($rootScope.User.GetAuthData().uid, false)
            .then(function (challengeData) {
                $scope.challenges = challengeData;
                $scope.$apply();
                $rootScope.hideLoading();
            }, function (error) {
                // Something went wrong.
                console.error(error);
                $rootScope.hideLoading();
            });
    };

    // Remove Challenge
    $scope.RemoveChallenge = function (challengeId) {
        challengeService.removeChallenge($rootScope.User.GetAuthData().uid, challengeId);
        $scope.init();
    };

    // Edit challenge
    $scope.Edit = function (challengeId, challengeName) {
        $scope.editChallenge = {id: challengeId, name: challengeName};

        // Popup to edit challenge
        var myPopup = $ionicPopup.show({
            template: '<input class="item myNumberInput" type="text" ng-model="editChallenge.name" placeholder="Challenge name" style="background: white;"/>',
            title: '<b>Edit the project name</b>',
            subTitle: 'You cannot edit the challenge without consent from the other party',
            scope: $scope,
            buttons: [
                {
                    text: '<i class="icon ion-close"></i>',
                    type: 'button',
                    onTap: function (e) {
                        return null;
                    }
                },
                {
                    text: '<i class="icon ion-checkmark"></i>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.editChallenge.name || !$scope.editChallenge.id) {
                            e.preventDefault();
                        }
                        else {
                            return $scope.editChallenge;
                        }
                    }
                }
            ]
        });

        $scope.init();

        $scope.$on("refreshChallenges", function () {
            $scope.init();
        })
    }
});
