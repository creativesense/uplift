'use strict';
angular.module('uplift').controller('challengeCommentController', function($scope, $rootScope, $stateParams, $timeout, $firebaseObject, challengeService, Users, ionicMaterialInk, ionicMaterialMotion, $ionicPopup, 
    $http, Auth, commentService, $ionicHistory, $cordovaCamera, $ionicListDelegate) {

    $scope.commentsParams = {}
    $scope.challengeId = $stateParams.id;
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');
    $scope.comments = commentService.getCommentsByChallengev2($scope.challengeId)
    console.log($scope.comments)

    $scope.createComment = function(commentsParams) {
      $scope.comments = []
        var comment = {};
        comment.challengeId = $scope.challengeId;
        comment.owner = $rootScope.User.GetAuthData().uid;
        comment.content = commentsParams.content;
        try {
              //debugger;
            commentService.createComment(comment);
        }catch(Exception) {
            console.log("Failed to add comment");
        }
    }

    $scope.data = {
    showDelete: false
  };
  
  $scope.edit = function(item) {
    alert('Edit Item: ' + item.id);
  };
  $scope.share = function(item) {
    alert('Share Item: ' + item.id);
    $ionicListDelegate.closeOptionButtons();  // this closes swipe option buttons after alert
  };
  
  $scope.moveItem = function(item, fromIndex, toIndex) {
    $scope.items.splice(fromIndex, 1);
    $scope.items.splice(toIndex, 0, item);
  };

  $scope.delItem = function(comment) {
    if(comment.owner == $rootScope.User.GetAuthData().uid) {
        commentService.deleteComment(comment.id, comment.challenge)
    }
    // $scope.items.splice($scope.items.indexOf(item), 1);
    $ionicListDelegate.closeOptionButtons();
  };

  
  $scope.onItemDelete = function(item) {
    $scope.items.splice($scope.items.indexOf(item), 1);
    $scope.data.showDelete = false;  // this closes delete-option buttons after delete
  };
  
  $scope.items = [];
  for (var i=0; i<30; i++) {
    $scope.items.push({ id: i});
  }

    



}
)
   

