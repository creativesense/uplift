'use strict';
angular.module('uplift').controller('challengeFollowerListController', function($scope, $rootScope, $stateParams, $timeout, $firebaseObject, challengeService, Users, ionicMaterialInk, ionicMaterialMotion, $ionicPopup, 
    $http, Auth, commentService, friendService, followerService, $ionicHistory, $cordovaCamera) {
    // Set Header
    $scope.challengeId = $stateParams.id;
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('right');

    $scope.followers = followerService.getAllFollowersFromChallenge($scope.challengeId);
})
   

