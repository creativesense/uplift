'use strict';
/**
 * Challenge Controller
 * Handles all the interactions in the challenge page.
 *
 * @package     challenge
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the  page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   $ionicHistory       Keeps track of the views navigated by the user.
 * @param   $http               
 * @param   $firebaseArray      The entire data stored in firebase.
 * @param   $firebaseObject     
 * @param   $cordovaCamera      
 * @param   challengeService          List of all the challenges.          
 * @param   Users               List of all the users.
 * @param   Auth                Handles the authorisation of the users.                      
 * @param   attachmentService         List of all the attachments.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 */

angular.module('uplift').controller('challengeController', function($scope, $state, $rootScope, $stateParams, $timeout, $ionicHistory, FDBRoot, 
                                                                    $http, $cordovaCamera, $cordovaCapture, challengeService, $ionicPopover,$firebaseArray, 
                                                                    Users, Auth, attachmentService, activityService, ionicMaterialMotion, ionicMaterialInk,
                                                                    $ionicPopup, $ionicModal) {
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";  
    var PENDING = "PENDING";
	var ARCHIVED = "ARCHIVED";
	var PENDINGCOMPLETIONAPPROVAL = "PENDINGCOMPLETIONAPPROVAL";
	var DECLINED = "DECLINED";
	var COMPLETED = "COMPLETED";
	var ACTIVE = "ACTIVE";
    $scope.showCount = 0;
    $scope.loggedUser = $rootScope.loggedUserId;
    $scope.challengeId = $stateParams.id;
    $scope.challenge = challengeService.getChallengeById($scope.challengeId);
    $scope.activity = activityService.getActivityByChallenge($scope.challengeId)
    $scope.status;
    $scope.statusComplete;
    $scope.completionResult;
    $scope.loggedUserIsChallengee;
    $scope.isFollowerAndChallenger;
    $scope.isFollowerAndChallengee;
    
    angular.forEach($scope.challenge.followers, function(value, key){
        if(value.id == $scope.challenge.challenger.id){
             $scope.isFollowerAndChallenger = true;
        }
        if(value.id == $scope.challenge.challengee.id){
             $scope.isFollowerAndChallengee = true;
        }
    })

    if($scope.challenge.status == PENDINGCOMPLETIONAPPROVAL){
        $scope.status == "PENDING COMPLETION APPROVAL"
    }else{
        $scope.status = $scope.challenge.status
    }

    if($scope.loggedUser == $scope.challenge.challengee.id){
        $scope.loggedUserIsChallengee = true;
    }else{
        $scope.loggedUserIsChallengee = false;
    }

    if($scope.challenge.status == COMPLETED){
         $scope.statusComplete = true;
    }else{
        $scope.statusComplete = false;
    }

    if($scope.challenge.successfulCompletion){
        $scope.completionResult = "Successfully Completed";
    }else{
        $scope.completionResult = "Unsuccessfully Completed";
    }

    $scope.activityElement = []
    $scope.detailsElement = []
    $scope.progressElement = []
    $scope.progressPercent = $scope.challenge.progress;
    $scope.progressVal

    $scope.updateProgress = function(progress){
        if(progress > 100 || progress < 0){
        }else{
            $scope.progressPercent = progress;
            challengeService.updateProgress(progress, $scope.challengeId)
        }
    }

    $scope.activityElement[0] = {
        name: "Activity",
        content: $scope.activity,
        size: $scope.activity.length
    }
    $scope.detailsElement[0] = {
        name: "Details",
        content: $scope.challenge,
        size: 1
    }
    $scope.progressElement[0] = {
        name: "Progress",
        content: [],
        size: 1,
        progress: 30
    }




     // A confirm dialog
    $scope.completeChallengePopup = function() {
    var confirmPopup = $ionicPopup.confirm({
        title: 'Complete Challenge',
        template: 'Are you sure you are done with the challenge?'
    });

    confirmPopup.then(function(res) {
        if(res) {
        $scope.completeChallenge();
        console.log('Completed');
        } else {
        console.log('You are not sure');
        }
    });
    };

        // An alert dialog
    $scope.showAlertChallengee = function() {
    var alertPopup = $ionicPopup.alert({
        title: 'Challenger Confirmation',
        template: 'A notification has been sent to the challenger to confirm your completion.'
     });
    alertPopup.then(function(res) {
        console.log('A notification has been sent to the challenger to confirm this.');
    });
    };

        // An alert dialog
    $scope.showAlertChallengerApproval = function(challengeeName) {
        var alertPopup = $ionicPopup.alert({
            title:  challengeeName+' has completed the challenge',
            template: challengeeName+' has completed the challenge and is awaiting your approval.'
        });
        alertPopup.then(function(res) {
            console.log('A notification has been sent to the challenger to confirm this.');
        });
    };

    $scope.completeChallenge = function(){
        var loggedUser = $rootScope.loggedUserId;
        if(loggedUser == $scope.challenge.challengerId){
            $scope.completeChallengePopup();
            challengeService.completeChallengeChallenger($scope.challenge.id)
        }else{
            // $rootScope.alert("Error", "Unauthorised");
        }if(loggedUser == $scope.challenge.challengeeId){
            challengeService.completeChallengeChallengee($scope.challenge.id)
            $scope.showAlertChallengee();
        }else{
            // $rootScope.alert("Error", "Unauthorised");
        }
    }

    $scope.terminateChallenge = function(loggedUser){
        
    }

			// var numdays = Math.floor(seconds / 86400);
			// var numhours = Math.floor((seconds % 86400) / 3600);
			// var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
			// var numseconds = ((seconds % 86400) % 3600) % 60;
			

    $scope.now = new Date();
    $scope.counter = $scope.challenge.deadline - $scope.now;
    $scope.daysCD = Math.floor($scope.counter / 86400);
    $scope.hoursCD = Math.floor(($scope.counter % 86400) / 3600);;
    $scope.minutesCD = Math.floor((($scope.counter % 86400) % 3600) / 60);
    $scope.secondsCD = (($scope.counter % 86400) % 3600) % 60;

    $scope.onTimeout = function(){
        $scope.counter--;
        if ($scope.counter > 0) {
            mytimeout = $timeout($scope.onTimeout,1000);
        }
        else {
            alert("Time is up!");
        }
    }
    var mytimeout = $timeout($scope.onTimeout,1000);
    
    $scope.reset= function(){
        $scope.counter = 5;
        mytimeout = $timeout($scope.onTimeout,1000);
    }
    
    $scope.init = function(){
        if($scope.challenge.status == PENDINGCOMPLETIONAPPROVAL) {
            if($rootScope.loggedUserId == $scope.challenge.challengerId) {
                // $scope.showAlertChallengerApproval($scope.challenge.challengeeName)
                //debugger;
            } else {
                console.log("Challenger not logged to confirm challenge completion");
            }     
        } else if($scope.challenge.status == COMPLETED || $scope.challenge.status == ARCHIVED){
            // debugger;
            // $state.go("app.challengeSummary", {id: $scope.challenge.id})
        } else{
            //Do nothing
        }
    }
    $scope.init();
    


    // console.log(JSON.stringify($scope.challenge, null, 4));
    // Delay expansion
    // $timeout(function() {
    //     $scope.isExpanded = true;
    //     $scope.$parent.setExpanded(true);
    // }, 300);

    $timeout(function () {
        $scope.showFabButton = false;
    }, 300);
 
    ionicMaterialInk.displayEffect();
    // $scope.challenger = Users.getUserById($scope.challenge.owner).GetDisplayName ();
    $scope.OtherShare = function(){
        window.plugins.socialsharing.share('I was challenged to ' + $scope.challenge.name + ", using the UPLIFT app. It is fun and easy to use. Make sure you try it out." , 'UPLIFT SA - For a better South Africa', null, 'http://upliftsa.co.za');
    }

        /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };


    var challengeReference = FDBRoot.child("challenge").child($scope.challengeId);
    
    var syncArray = $firebaseArray(challengeReference.child("images"));
    $scope.images = syncArray;
    
    $scope.upload = function() {
        var options = {
            quality : 100,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: true
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            syncArray.$add({image: imageData, date: Firebase.ServerValue.TIMESTAMP}).then(function() {
                alert("Image has been uploaded");
            });
        }, function(error) {
            console.error(error);
        });

        activityService.createActivity($scope.challengeId, $rootscope.loggedUserId, "MEDIA", "ADDED")

    };

    var vidArray = $firebaseArray(challengeReference.child("videos"));
    $scope.videos = vidArray;

    //  $scope.record = function() {
    //     var options = {
    //         quality : 100,
    //         destinationType : Camera.DestinationType.DATA_URL,
    //         sourceType : Camera.PictureSourceType.CAMERA,
    //         allowEdit : true,
    //         mediaType: 'VIDEO',
    //         popoverOptions: CameraPopoverOptions,
    //         targetWidth: 500,
    //         targetHeight: 500,
    //         saveToPhotoAlbum: true
    //     };

    //     $cordovaCamera.getPicture(options).then(function(videoData) {
    //         vidArray.$add({video: videoData, date: Firebase.ServerValue.TIMESTAMP}).then(function() {
    //             alert("Video has been uploaded");
    //         });
    //     }, function(error) {
    //         console.error(error);
    //     });
    // };   


    $scope.record = function(){
      var options = { limit: 1, duration: 15 };
      $cordovaCapture.captureVideo(options).then(
          function(videoData) {
              var i, path, len;
              var pathtogo;
              var pathtogostring;
              for (i = 0, len = videoData.length; i < len; i += 1) {
                  path = videoData[i].fullPath;
                  pathtogo = path.toString();
                  pathtogostring = pathtogo.substr(6);  
                  alert("Path of the video is = " + path.toString());                                      
                  obj = {
                      videoP: path,
                      videosrc: pathtogostring
                  }
                  $scope.videos.$add(obj);
                }
          },
          function(err) {
          }
      );
    }
    
    
    
    $scope.showImages = function(index) {
        $scope.activeSlide = index;
        $scope.showModal('templates/image-popover.html');
    }

    $scope.playVideo = function() {
        $scope.showModal('templates/video-popover.html');
    }

    $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    // Close the modal
    $scope.closeModal = function() {
        $scope.modal.hide();
        $scope.modal.remove()
    };  
})


angular.module('uplift').controller('ChallengeCtrlFabButton', function($scope, $rootScope, $stateParams, $timeout, Auth, $firebaseArray, ionicMaterialInk, ionicMaterialMotion, $ionicPopup, $ionicHistory, challengeService) {
    $timeout(function () {
        $scope.showFabButton = true;
    }, 900);

    $ionicHistory.clearHistory();

    $scope.images = [];
    
    var fbAuth = Auth.getAuth();
    if(fbAuth) {
        // var userReference = Users.User(fbAuth.uid);
        var challengeReference = challengeService.getChallengeById($stateParams.id);
        var syncArray = $firebaseArray(challengeReference.child("images"));
        $scope.images = syncArray;
    } else {
        $state.go("firebase");
    }

    $scope.upload = function() {
        var options = {
            quality : 100,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: true
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            syncArray.$add({image: imageData}).then(function() {
                alert("Image has been uploaded");
            });
        }, function(error) {
            console.error(error);
        });
    };
})

    

angular.module('uplift').controller("AttachmentCtrl", function($scope, $stateParams, $timeout, Auth, Users, FDBRoot, challengeService, attachmentService, ionicMaterialInk, ionicMaterialMotion, $ionicHistory, $firebaseArray, $cordovaCamera) {

    $ionicHistory.clearHistory();
    $scope.images = [];
    
    var fbAuth = Auth.getAuth();
    if(fbAuth) {
        var userReference = Users.User(fbAuth.uid);
        var syncArray = $firebaseArray(userReference.child("images"));
        $scope.images = syncArray;
    } else {
        $state.go("firebase");
    }

    $scope.upload = function() {
        var options = {
            quality : 100,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            syncArray.$add({image: imageData}).then(function() {
                alert("Image has been uploaded");
            });
        }, function(error) {
            console.error(error);
        });
    }

            /**
         * STARTNOTIF
         */        
            // FDBRoot.child("user").child($rootScope.loggedUserId).on("value", function(snapshot3){
            //     var target = []
            //     angular.forEach(challenge.followers, function(value, key){
            //         target.push(value.id)
            //     })
            //     var user = snapshot3.val();
            //         FDBRoot.child("user").child(follower).on("value", function(snapshot4){
            //         var follower = snapshot4.val();    
            //         var message = user.name + " added " + follower.name + "as a follower"
            //         var type = "Follower"
            //         var owner = user.id
            //         var uisref = "app.challenge.followers"
            //         notificationService.createNotificationChallengeFollower(challengeId, owner, type, message, target, uisref)
            //     })                
            // });      
        /**
         * END NOTIF
         */


})