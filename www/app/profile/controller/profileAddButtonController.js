'use strict';

/**
 * ProfileAddButton Controller
 * Handles all the interactions in the profileAddButton page.
 *
 * @package     profile
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   Challenges          List of all the challenges.
 * @param   User                User currently logged in.
 * @param   Users               List of all the users.
 * @param   ionicMaterialMotion Motion from the Ionic-Material templat.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 * @param   $ionicModal         Enables the page to create content panes that go over the main view temporarily.
 */

angular.module('uplift').controller('profileAddButtonController', function ($scope, $rootScope, $stateParams, $timeout, challengeService,
                                                                            User, Users, friendService, charityService, ionicMaterialInk, ionicMaterialMotion, $ionicPopup, $ionicModal, $location) {

    //region Create Challenge Modal
    //region UI data
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.challengeParams = {}
        $scope.$parent.setHeaderFab('left');
        $timeout(function () {
        $scope.showFabButton = true;
    }, 300);

        $scope.baseUser = {
                name: "",
                email: "",
                profileImageURL: "",
                lastLoggedIn: "",
                memberSince: ""
        };

        //$scope.loggedUser = Users.getUserByIdFirebaseObject($rootScope.User.GetAuthData().uid);
        $scope.charities = charityService.getAllCharities();
        $scope.challengeCategories = //challengeCategoriesFirebase;
        //$scope.friends = $scope.loggedUser.friends;

        /**
         * Creates a challenge from the modal form
         * @param arrayObjects
         */
        $scope.createChallenge = function (arrayObjects) {
                var tempChallenge = {};
                //Check if email is valid

                //check if friends

                //if friends create challenge
                var challengeeId = $scope.getIdFromEmail(arrayObjects.challengee);
                console.log("THis is your challengeesId" + challengeeId);
                if (!challengeeId) {
                        return null
                } else {
                    //debugger;
                        tempChallenge.owner = $rootScope.User.GetAuthData().uid;
                        tempChallenge.challengee = challengeeId;
                        tempChallenge.name = arrayObjects.name;
                        tempChallenge.category = arrayObjects.category.name;
                        tempChallenge.description = arrayObjects.description;
                        tempChallenge.bounty = arrayObjects.bounty;
                        tempChallenge.charityPercentage = 20;//arrayObjects.charityPercentage;
                        tempChallenge.charity = arrayObjects.charity;
                        tempChallenge.deadline = arrayObjects.deadline;
                        tempChallenge.image = null;
                        challengeService.createChallenge(tempChallenge);
                }

                $rootScope.$broadcast("refreshChallenges");
        };

        /**
         * Searches for a userByEmail
         * @param email
         * @returns {boolean}
         */
        $scope.isUser = function (email) {
                var key = Users.getUserByEmail(email);
                if (!key) {
                        return false;
                } else {
                        return true;
                }
        };

        /**
         * From the users email, obtain their id
         * @param email
         * @returns {*}
         */
        $scope.getIdFromEmail = function (email) {
                var id = Users.getUserIdByEmail2($scope.metaData, email)
                if (!id) {
                        return null;
                } else {
                        return id;
                }
        };

        /**
         * Get User byId
         * @param id
         */
        $scope.getUser = function(id) {
                var ref = new Firebase("https://upliftsa.firebaseio.com/users-metadata/" + id);
                ref.on("value", function(snapshot) {
                        var response = snapshot.val();
                        $scope.baseUser.name = response.name;
                        $scope.baseUser.email = response.email;
                        $scope.baseUser.profileImageURL = response.profileImageURL;
                        $scope.baseUser.lastLoggedIn = response.updated;
                        $scope.baseUser.memberSince = response.created;
                }, function (errorObject) {
                        console.log("The read failed: " + errorObject.code);
                });
        };

        /**
         * Obtain Username from meta-data
         * @param userId
         */
        $scope.getUserName = function(userId) {
                var ref = new Firebase("https://upliftsa.firebaseio.com/users-metadata/" + userId);
                ref.on("value", function(snapshot) {
                        var response = snapshot.val();
                        $scope.baseUser.name = response.name;
                        console.log($scope.baseUser.name);
                        return $scope.baseUser.name;
                }, function (errorObject) {
                        console.log("The read failed: " + errorObject.code);
                });
        };

        /**
         * Fetches all the charities to select from
         */
        //endregion



    //$scope.preLoadedCharities = $scope.getCharities();
    //endregion

        $scope.modal = $ionicModal.fromTemplate(
        '<ion-modal-view>' +
            ' <ion-header-bar class = "positive">' +
                '<h1 class = "title">Create Challenge</h1>' +
            '</ion-header-bar>' +
            '<ion-content>'+
                '<form role="form">'+
                '<ion-list>'+
        '<ion-item>'+
            '<div class="list">' +
                '<label class="item item-input item-select">' +
                    '<div class="input-label">' +
                        'Challenge Category' +
                    '</div>' +
                    '<select name ="" ng-model="challengeParams.category" ng-options="obj1.name for obj1 in challengeCategories">' +
                    '</select>' +
        '       </label>' +
            '</div>' +
        '</ion-item>'+
        '<ion-item>'+
                '<div class="list">' +
                    '<label class="item item-input item-select">' +
                        '<div class="input-label">' +
                            'Challengee' +
                        '</div>' +
                    '<select name ="" ng-model="challengeParams.challengee" ng-options="obj1.$id for obj1 in friends">' +
                    '</select>' +
                    '</label>' +
                '</div>' +
        '</ion-item>'+
        //             '<ion-item>'+
        //                 '<label class="item item-input" ion-item>'+
        // '<input type="text" placeholder="Challengee Email: someone@something.com" ng-model="challengeParams.challengee" /> </label>' +
        //             '</ion-item>'+
                    '<ion-item>'+
                        '<label class="item item-input" ion-item>'+
        '<input type="text" placeholder="Name: Run a 5K" ng-model="challengeParams.name" /> </label>' +
                    '</ion-item>'+
                    '<ion-item>'+
                        '<label class="item item-input" ion-item>'+
        '<input type="text" placeholder="Description: Run a 5km in under 30 minutes, post your time on the S-Health app" ng-model="challengeParams.description" /> </label>' +
                    '</ion-item>'+
                    '<ion-item>'+
                        '<label class="item item-input" ion-item>'+
        '<input type="number" placeholder="Bounty: The amount the challenge carries e.g.200" ng-model="challengeParams.bounty"/>  </label>' +
                    '</ion-item>'+
                    '<ion-item>'+
                        '<label class="item item-input" ion-item>'+
        '<input type="date" placeholder="Deadline: 10/12/1017" ng-model="challengeParams.deadline"/> </label>' +
                    '</ion-item>'+
                    '<ion-item>'+
                        '<div class="list">' +
                                '<label class="item item-input item-select">' +
                                                '<div class="input-label">' +
                                                'Charity' +
                                                '</div>' +
        '<select name ="" ng-model="challengeParams.charity" ng-options="obj1 for obj1 in charityNames">' +
                                '</select>' +
                                '</label>' +
                        '</div>' +
                 '</ion-item>'+
                '</ion-list>'+
        '<button class = "button icon icon-left ion-ios-close-outline" ng-click = "createChallenge(challengeParams)">Create</button>' +
        '<button class = "button icon icon-left ion-ios-close-outline" ng-click = "closeCreateChallengeModal()">Cancel</button>' +

        '</ion-content>' +
                '</form>'+
   '</ion-modal-view>', {
      scope: $scope,
      animation: 'slide-in-up'
        });

   //Opens the challenge modal
    $scope.openCreateChallengeModal = function () {
      $scope.modal.show();
   };
	//Closes the challenge modal
    $scope.closeCreateChallengeModal = function () {
      $scope.modal.hide();
        $location.path('/profile');
   };

        //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    //endregion


    //region Controller Methods




});