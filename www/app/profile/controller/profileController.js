'use strict';

/**
 * Profile Controller
 * Handles all the interactions in the profile page.
 *
 * @package     profile
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 *
 *
 * @param   $scope              Handles the functions and models applied in the page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   Challenges          List of all the challenges.
 * @param   Users               List of all the users.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 * @param   $ionicModal         Enables the page to create content panes that go over the main view temporarily.
 */

angular.module('uplift').controller('profileController', function ($scope, $rootScope, $stateParams, $timeout, userService,
                                                                   Users, challengeService, friendService, firebaseUserService, genericService,
                                                                    ionicMaterialMotion, notificationService, ionicMaterialInk,
                                                                    $ionicPopup, $ionicModal) {
    // $scope.challenges = [];
    $scope.background = $rootScope.background;
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);
    $rootScope.loggedUserId = $rootScope.User.GetAuthData().uid;
    //$scope.loggedUser = $rootScope.User;
    $scope.loggedUser = userService.getUserById($rootScope.loggedUserId);
    $scope.profileImage = $rootScope.User.GetProfileImageURL();
    $scope.name = $rootScope.User.GetDisplayName();
    $scope.email = $rootScope.User.GetEmail();
    $scope.username = userService.getUsernameById($rootScope.loggedUserId);
    $scope.completedChallenges = $scope.loggedUser.completedChallenges;
    $scope.ongoingChallenges  = $scope.loggedUser.ongoingChallenges;
    $scope.failedChallenges  = $scope.loggedUser.failedChallenges;
    $scope.successfulChallenges = $scope.loggedUser.successfulChallenges;
    $scope.bounty  = $scope.loggedUser.bounty;
    $scope.notifications = notificationService.getNotificationByUser($rootScope.loggedUserId)
    $scope.friendCount = friendService.getFriendCount($rootScope.loggedUserId);
    $scope.donatedToCharity = $scope.bounty * 0.15
    $scope.completed = "COMPLETED"
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    $scope.profileImage = {
        'background-image': 'url(' + $rootScope.User.GetProfileImageURL() + ')'
    };

    // Set Motion
    $timeout(function () {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });
    }, 300);

    // Set Ink
    ionicMaterialInk.displayEffect();

    $scope.createdChallenges = challengeService.getCreatedChallenges($rootScope.loggedUserId);
    $scope.challengedChallenges = challengeService.getChallengedChallenges($rootScope.loggedUserId);
    $rootScope.challengeCreatedCount = $scope.createdChallenges.length;
    $rootScope.challengedChallengesCount = $scope.challengedChallenges.length;
    $scope.totalChallenges = $rootScope.challengeCreatedCount+ $rootScope.challengedChallengesCount
    
    $scope.challenges = [];
    $scope.challenges[0] = {
      name: "Created Challenges",
      challenges : $scope.createdChallenges,
      size : $scope.createdChallenges.length
    }

    $scope.challenges[1] = {
      name: "Received Challenges",
      challenges : $scope.challengedChallenges,
      size : $scope.challengedChallenges.length
    }
    $scope.statistics = [];
    $scope.innerStats = [];
    $scope.innerStats[0] = {
      name: "Amount Donated",
      value :  $scope.donatedToCharity
    }
    $scope.innerStats[1] = {
      name: "Friend Count",
      value :  $scope.bounty
    }
    $scope.innerStats[2] = {
      name: "Complete Challenges",
      value : $scope.completedChallenges
    }
    $scope.innerStats[3] = {
      name: "Successful Challenges",
      value : $scope.successfulChallenges
    }
    $scope.innerStats[4] = {
      name: "Total Bounty",
      value :  $scope.bounty
    }
    // $scope.innerStats[4] = {
    //   name: "Total Challenges",
    //   value :  $scope.totalChallenges
    // }
    // $scope.innerStats[5] = {
    //   name: "Number of Friends",
    //   value :  $scope.friendCount
    // }
    $scope.statistics[0] = {
      name: "Statistics",
      value : $scope.innerStats,
    }

$scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  
  console.log($scope.createdChallenges);



    // Load user challenges and initialise page
    // $scope.init = function () {
    //     $rootScope.showLoading("Loading data...");
    //     challengeService.getChallengedChallenges($rootScope.loggedUserId)
    //         .then(function(groupData) {
    //             $scope.challenges = groupData;
    //             $scope.$apply();
    //             $rootScope.hideLoading();
    //         }, function(error) {
    //             // Something went wrong.
    //             console.error(error);
    //             $rootScope.hideLoading();
    //         });
    //     $rootScope.hideLoading();
    // };

    // $scope.init();

    // $scope.$on("refreshChallenges", function () {
    //     $scope.init();
    // });
});
