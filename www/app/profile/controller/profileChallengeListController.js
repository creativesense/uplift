'use strict';

/**
 * Profile Controller
 * Handles all the interactions in the profile page.
 *
 * @package     profile
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 *
 *
 * @param   $scope              Handles the functions and models applied in the page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   Challenges          List of all the challenges.
 * @param   Users               List of all the users.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 * @param   $ionicModal         Enables the page to create content panes that go over the main view temporarily.
 */

angular.module('uplift').controller('profileChallengeListController', function ($scope, $rootScope, $stateParams, $timeout,
                                                                   Users, challengeService, friendService, genericService,
                                                                    ionicMaterialMotion, ionicMaterialInk,
                                                                    $ionicPopup, $ionicModal, FDBRoot) {
    
    $rootScope.loggedUserId = $rootScope.User.GetAuthData().uid;
    $scope.createdChallenges = challengeService.getCreatedChallenges($rootScope.loggedUserId);
    console.log(JSON.stringify($scope.createdChallenges, null, 4));
    $scope.challengedChallenges = challengeService.getChallengedChallenges($rootScope.loggedUserId);
    $rootScope.challengeCount = $scope.challengedChallenges.length;
    $scope.challenges = [];
    $scope.challenges[0] = {
      name: "Created Challenges",
      challenges : $scope.createdChallenges,
      size : $scope.createdChallenges.length
    }

    $scope.challenges[1] = {
      name: "Received Challenges",
      challenges : $scope.challengedChallenges,
      size : $scope.challengedChallenges.length
    }

    $scope.data = {
    showDelete: false
  };

  // $scope.purifiedChallenges = challengeService.getChallenges2($rootScope.loggedUserId)
  // console.log($scope.purifiedChallenges);
  
  $scope.edit = function(item) {
    alert('Edit Item: ' + item.id);
  };
  $scope.share = function(item) {
    alert('Share Item: ' + item.id);
    $ionicListDelegate.closeOptionButtons();  // this closes swipe option buttons after alert
  };
  
  $scope.moveItem = function(item, fromIndex, toIndex) {
    $scope.items.splice(fromIndex, 1);
    $scope.items.splice(toIndex, 0, item);
  };

  $scope.delItem = function(challenge) {
    if(challenge.challenger == $rootScope.User.GetAuthData().uid) {
        challengeService.removeChallenge(challenge)
    }
    // $scope.items.splice($scope.items.indexOf(item), 1);
    $ionicListDelegate.closeOptionButtons();
  };
 
  $scope.onItemDelete = function(item) {
    $scope.items.splice($scope.items.indexOf(item), 1);
    $scope.data.showDelete = false;  // this closes delete-option buttons after delete
  };
  
  $scope.items = [];
  for (var i=0; i<30; i++) {
    $scope.items.push({ id: i});
  }

    /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

});
