'use strict';

/**
 * Friends Controller
 * Handles all the interactions in the friends page.
 *
 * @package     friends
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the friends page.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 */

angular.module('uplift').controller('followerController', function ($scope, $rootScope, $stateParams, $timeout, 
ionicMaterialInk, ionicMaterialMotion, followerService, notificationService, challengeService, FDBRoot, friendService, Users, $state, $location)
{
    // Set Header
    //$rootScope.loggedUser = $rootScope.User.GetAuthData().uid;
    $scope.challengeId = $stateParams.challengeId;
    $scope.followers =  Users.getAll();;
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('left');

    $scope.addFollower = function(follower){
        var challenge = challengeService.getChallengeById($scope.challengeId)
        var response = followerService.addFollower(follower, $scope.challengeId);
        $rootScope.alert(response.title, response.message);
        /**
         * STARTNOTIF
         */        
            FDBRoot.child("user").child($rootScope.loggedUserId).on("value", function(snapshot3){
                var target = []
                angular.forEach(challenge.followers, function(value, key){
                    target.push(value.id)
                })
                var user = snapshot3.val();
                    FDBRoot.child("user").child(follower).on("value", function(snapshot4){
                    var follower = snapshot4.val();    
                    var message = user.name + " added " + follower.name + "as a follower"
                    var type = "Follower"
                    var owner = user.id
                    var uisref = "app.challenge.followers"
                    notificationService.createNotificationChallengeFollower($scope.challengeId, owner, type, message, target, uisref)
                })                
            });      
        /**
         * END NOTIF
         */
    }

    // Delay expansion
    $timeout(function() {
        $scope.isExpanded = true;
        //$scope.$parent.setexpanded(true);
    }, 300);
    // Set Motion
    //ionicMaterialMotion.fadeSlideInRight();
    // Set Ink
    ionicMaterialInk.displayEffect();
})