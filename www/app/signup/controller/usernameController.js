'use strict';
/**
 * Challenge Controller
 * Handles all the interactions in the challenge page.
 *
 * @package     username
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 *           
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the  page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   $ionicHistory       Keeps track of the views navigated by the user.
 * @param   $http               
 * @param   $firebaseArray      The entire data stored in firebase.
 * @param   $firebaseObject     
 * @param   $cordovaCamera      
 * @param   challengeService          List of all the challenges.          
 * @param   Users               List of all the users.
 * @param   Auth                Handles the authorisation of the users.                      
 * @param   attachmentService         List of all the attachments.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 */

angular.module('uplift').controller('usernameController', function($scope, $rootScope, $stateParams, $timeout, $ionicHistory, $state,
                                                                    $http, $cordovaCamera, Users, Auth, userService, ionicMaterialMotion, ionicMaterialInk,
                                                                    $ionicPopup) {  
    //Set Header
    // $scope.$parent.showHeader();
    // $scope.$parent.clearFabs();
    // $scope.isExpanded = false;
    // $scope.$parent.setExpanded(false);
    // $scope.$parent.setHeaderFab(false);
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    $scope.myObj = {
        "color" : "white",
        "background-color" : "coral",
        "font-size" : "100px",
        "padding" : "50px"
    }

    $scope.loggedUser = $rootScope.loggedUserId = $rootScope.User.GetAuthData().uid;
    $scope.user = {
        username: ""
    };
    $scope.usernameRegex = /^[a-z][a-z0-9]*([_-][a-z0-9]+)*$/

    $scope.updateUsername = function(usernameFromForm){
        var username = $scope.user.username;
        if(!username){
            var errorMessage = "Your username should be greater than 3 and less than 15 characters/n Your username can only contain '_''-'!"
            $rootScope.alert("Username Error", errorMessage)    
        }else {
                 userService.createUsername($scope.loggedUser, username);
            }     
        }   

    // Delay expansion
    /*$timeout(function() {
        $scope.isExpanded = true;
        $scope.$parent.setExpanded(true);
    }, 300);*/

    $timeout(function () {
        $scope.showFabButton = true;
    }, 300);
 
    ionicMaterialInk.displayEffect();
    $ionicHistory.clearHistory();
})

