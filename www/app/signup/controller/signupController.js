'use strict';

/**
 * SignUp Controller
 * Handles all the interactions in the signup page page.
 *
 * @package     signup
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>, Martin Ombura <martin.omburajr@gmail.com>,
 *              Ndakondja Shilenga <ndasindana@gmail.com>, Mujahid Dollie <mujahidollie@gmail.com
 * @copyright   Copyright (c) 2016 Creative Sense.
 * 
 * 
 * @param   $scope              Handles the functions and models applied in the friends page.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 */

angular.module('uplift').controller('signupController', function ($scope,$timeout, Auth, userService, ionicMaterialInk, ionicMaterialMotion, $ionicSideMenuDelegate){
    
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";

    // Disable the Side menu on the page
    $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
    });


    // Set user email and password to empty strings
    $scope.user =
    {
        username:"",
        email:"",
        password:""
    }

    // Create the user
    $scope.signupEmail= function(){   
        var email =  $scope.user.email;
        var password = $scope.user.password;
        return Auth.createUser(email,password)    
    };



})