'use strict';

/**
 * Friend Profile Controller
 * Handles all the interactions in the profile page.
 *
 * @package     profile
 * @author      Martin Ombura <martin.omburajr@gmail.com>,
 * @copyright   Copyright (c) 2016 Creative Sense.
 *
 *
 * @param   $scope              Handles the functions and models applied in the page.
 * @param   $rootScope          Handles the functions and models applied in the application.
 * @param   $stateParams        Passes in the navigated url.
 * @param   $timeout            Function that performs an operation after a giving time.
 * @param   Challenges          List of all the challenges.
 * @param   Users               List of all the users.
 * @param   ionicMaterialMotion Motion from the Ionic-Material template.
 * @param   ionicMaterialInk    Ink from Ionic-Material template.
 * @param   $ionicPopup         Allows the page to display popups.
 * @param   $ionicModal         Enables the page to create content panes that go over the main view temporarily.
 */

angular.module('uplift').controller('friendsProfileController', function ($scope, $rootScope, $stateParams, $ionicHistory, $timeout,
                                                                   Users, friendService,userService,
                                                                   ionicMaterialMotion, ionicMaterialInk, $ionicPopup, $ionicModal) {
    $scope.background = "img/" + Math.floor((Math.random() * 20) + 1) + ".jpg";
    var friendId = $stateParams.friendId;
    $scope.friend = userService.getUserById(friendId);
    $scope.bounty = $scope.friend.bounty;
    $scope.completedChallenges = $scope.friend.completedChallenges;
    $scope.successfulChallenges = $scope.friend.successfulChallenges;
    $scope.friendCount = friendService.getFriendCount(friendId);
    $scope.donatedToCharity = $scope.bounty * 0.15
    // $scope.myCreatedChallengesForFriends = relatedChallengesFirebase.createdByMeToFriend;
    // $scope.friendsCreatedChallengesForMe = relatedChallengesFirebase.createdByFriendToMe;
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);
    //$scope.MetaData = $rootScope.User.GetMetadata();
    $scope.userForRemoveFromChallenge = null;
    // $scope.profileImage = {
    //     'background-image': 'url(' + $scope.friend._meta.profileImageURL + ')'
    // };

    // Set Motion


    $scope.statistics = [];
    $scope.innerStats = [];
    $scope.innerStats[0] = {
      name: "Amount Donated",
      value :  $scope.donatedToCharity,
    }
    $scope.innerStats[1] = {
      name: "Friend Count",
      value :  $scope.bounty,
    }
    $scope.innerStats[2] = {
      name: "Complete Challenges",
      value : $scope.completedChallenges,
    }
    $scope.innerStats[3] = {
      name: "Successful Challenges",
      value : $scope.successfulChallenges,
    }
    $scope.innerStats[4] = {
      name: "Total Bounty",
      value :  $scope.bounty,
    }
    $scope.innerStats[5] = {
      name: "Number of Friends",
      value :  $scope.friendCount,
    }
    $scope.statistics[0] = {
      name: "Statistics",
      value : $scope.innerStats,
    }

        $timeout(function () {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });
    }, 150);

    $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

});

