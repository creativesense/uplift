    'use strict';
	/**
	 * Contains all functionality for charities
	 * @author      Martin Ombura <martin.omburajr@gmail.com>
	 * @copyright   Copyright (c) Creative Sense.
	 */


angular.module('charityService', ['firebase'])
	.factory('charityService', ['FDBRoot', '$firebaseArray', '$firebaseObject', function (FDBRoot, $firebaseArray, $firebaseObject) {
		var charityRef = FDBRoot.child('charity');
		return {

			/**
			 * Obtains a charity based on their Id
			 */
			getCharityById: function(charityId) {
				var ref = FDBRoot.child('charity').child(charityId);
				return $firebaseObject(ref);
			},
		
			/**
			 * Obtains all the charities
			 */
			getAllCharities: function () {
				return $firebaseArray(charityRef);
			}
		}
	}]);