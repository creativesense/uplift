angular.module('friendService', ['firebase'])
/**
 * Friend-relations
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
.factory('friendService', ['FDBRoot', 'Users', 'challengeService', '$firebaseObject', '$firebaseArray', function(FDBRoot, Users, challengeService, $firebaseObject, $firebaseArray) {
	var userRef = FDBRoot.child("user");
	var retObj = {};
	var PENDING = "PENDING";
	const ACCEPTED = "ACCEPTED";
	const DECLINED = "DECLINED";
	
    /**
     * Get a loggedInUsers friends
     */
	retObj.getFriends = function(userId) {
		var result = []
		var ref = FDBRoot.child("friend-by-user").child(userId);
		ref.on("value", function(snapshot){
			var friends = snapshot.val();
			angular.forEach(friends, function(value, key) {
					var userRef = FDBRoot.child("user").child(key);
					userRef.on("value", function(snapshot2){
						result.push(snapshot2.val())
					})
			})
		})
		return result;
	}

	retObj.getFriendCount = function(userId){
		var result = null;
		var ref = FDBRoot.child("friend-by-user").child(userId);
		ref.on("value", function(snapshot){
			var friends = snapshot.val();
			if(friends == null || friends == undefined){
				rresult = 0;
			} else{
				result = friends.length
			}			
		})
		return result;
	}

	/**
	 * Checks to see if a person is a friend of the requesting party
	 * @param userId
	 * @param friendId
	 * @returns {boolean}
	 */
	retObj.isFriend = function(userId, friendId) {
		var ref = FDBRoot.child("friend-by-user").child(userId);
		ref.on("value", function(snapshot){
			var result = snapshot.val();
			if(result == null) {
				return false;
			} else{
				return true;
			}
		})
	}


	/**
	 * Removes a friend
	 * @param userId
	 * @param friendId
     */
	retObj.removeFriend = function (userId, friendId) {

		var userFriendRef = FDBRoot.child("friend-by-user").child(userId).child(friendId);
		userFriendRef.remove();
		var friendUserRef = FDBRoot.child("friend-by-user").child(friendId).child(userId);
		friendUserRef.remove();
	},

	/**
	 * Gets a friend
	 * @param userId
	 * @param friendId
     */
	retObj.getFriend = function (userId, friendId) {
		var result = null;
		friendRef = FDBRoot.child("user").child(friendId)
		friendRef.on("value", function(snapshot2){
			result = snapshot2.val();
		})
		return result;
	},

	/**
	 * Gets the logged in user friendlist
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getAllFriends = function (userId) {
		return $firebaseArray(userRef.child(userId).child("friends"));
	}

	return retObj;
}])

