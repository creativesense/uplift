'use strict';

/**
 * Data Service whose purpose
 * Houses the logic and interactions with the modules of the application.
 *
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */

angular.module('uplift.data', ['firebase'])

// Users Factory
	.factory('Users', ['FDBRoot', '$firebaseArray', '$firebaseObject', function (FDBRoot, $firebaseArray, $firebaseObject) {
	var userRef = FDBRoot.child("user");
	// var userMetadataRef = FDBRoot.child("user-metadata");
	var userByEmailRef = FDBRoot.child("user-byemail");
	var retObj = {};

	/**Get all user
	 *
	 * @returns {*}
	 * @constructor
     */
	retObj.getAll = function () {
		return $firebaseArray(userRef);
	};

	retObj.getUserById = function(userId) {
		return userRef.child(userId);
	};
	/**Get user by Id
	 *
	 * @param userId
	 * @returns {XMLList}
     */
	// retObj.getUserById = function(userId) {
	// 	return $firebaseObject(userRef.child(userId));
	// };


	/**Get user by Id
	 *
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getUserByIdFirebaseObject = function(userId) {
		return $firebaseObject(userRef.child(userId));
	};

	/**Get user's metadata
	 *
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getUserMetaData = function(userId) {
		var userMetaDataRef = FDBRoot.child('user').child(userId);
		return $firebaseObject(userMetaDataRef);
	};


	/**Get user by email
	 *
	 * @param emailKey
	 * @returns {XMLList}
	 * @constructor
     */
	retObj.getUserIdByEmail = function (emailKey) {
		return userByEmailRef.child(emailKey);
	};

	/**Invite user to challenge
	 *
	 * @param emailAddress
	 * @param challengeId
	 * @param userId
     * @returns {*}
     */
	retObj.inviteUserToChallengeByEmail = function(emailAddress, challengeId, userId) {
		if (!emailAddress || !challengeId || !userId) return;

		return userByEmailRef.child(retObj.EmailToKey(emailAddress))
			.child("invites")
			.child(challengeId)
			.transaction(
                function(currentData) {
                	if (currentData === null) {
                		return {
							fromUserId: userId,
							challengeId: challengeId,
							created: Firebase.ServerValue.TIMESTAMP
						};
                	} else {

                	}
                }
            );
	};

	/**Check if email is duplicated
	 *
	 * @param emailAddress
	 * @returns {string}
     */
	retObj.emailToKey = function(emailAddress) {
        if(emailAddress) {
            return btoa(emailAddress);
        } else {
            throw new Error("_emailToKey(): Argument emailAdrress is not set!");
        }
    };



	return retObj;
}])