    'use strict';
	/**
	 * Contains all functionality for activity
	 * @author      Martin Ombura <martin.omburajr@gmail.com>
	 * @copyright   Copyright (c) Creative Sense.
	 */


angular.module('activityService', ['firebase'])
	.factory('activityService', ['FDBRoot', '$firebaseArray', '$firebaseObject', function (FDBRoot, $firebaseArray, $firebaseObject) {
		var activityRef = FDBRoot.child('activity');
        var retObj = {};

			/**
			 * Obtains a activity based on their Id
			 */
			retObj.getActivityById = function(activityId) {
                var tempActivity = {}
				var ref = FDBRoot.child('activity').child(activityId);
				ref.on("value", function(snapshot){
                    var activity = snapshot.val();
                    tempActivity.title = activity.type;                  
                    var userRef = FDBRoot.child("user").child(activity.owner).on("value", function(snapshot2){
                        tempActivity.owner = snapshot2.val();
                        tempActivity.action = activity.action;
                        tempActivity.type = activity.type;
                        tempActivity.created = activity.created;
                        tempActivity.sentence = tempActivity.owner.name + " " + activity.action + " a " + activity.type
                    })    
                })
                return tempActivity;
			}
		
			/**
			 * Creates an activity
			 */           
            retObj.createActivity = function(challengeId, userId, type, action){
                    var activityChallengeRef = FDBRoot.child("activity-by-challenge").child(challengeId)
                    var activityChallenge = activityChallengeRef.push()
                    var activityChallengeId = activityChallenge.key()
                    activityChallengeRef.child(activityChallengeId).set({
                        id: activityChallengeId
                    })
		            var activityRef = FDBRoot.child("activity").child(activityChallengeId);
                    activityRef.set({
                        id: activityChallengeId,
                        type: type,
                        action: action,
                        created: Firebase.ServerValue.TIMESTAMP,
                        owner: userId
                    })

                    var activityUserRef = FDBRoot.child("activity-by-user").child(userId).child(activityChallengeId);
                    activityUserRef.set({
                        id:activityChallengeId
                    })
                    return activityChallengeId;
            }

           retObj.getActivityByChallenge = function(challengeId){
                var result = []
                var tempActivity = {}
                var activityChallengeRef = FDBRoot.child("activity-by-challenge").child(challengeId)
                    activityChallengeRef.on("value", function(snapshot){
                        var activityChallenges = snapshot.val();
                        angular.forEach(activityChallenges, function(value, key){
                            var tempResult = retObj.getActivityById(key)
                            result.push(tempResult);
                        })
                    })
                return result;
            }
		
        return retObj;
	}])