'use strict';

angular.module('challengeRequestService', ['firebase'])
/**
 * Facilitates the creation of challengerequests
 * Challenge Request Statuses:	PENDING, ACCEPTED, DECLINED, REMOVED
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
	.factory('challengeRequestService', ['FDBRoot', 'Users', 'genericService', 'notificationService', 'challengeService', '$firebaseArray', '$firebaseObject', 
                                                    function (FDBRoot, Users, genericService, notificationService, challengeService, $firebaseArray, $firebaseObject) {
	var userRef = FDBRoot.child("user");
	var retObj = {};
	var PENDING = "PENDING";
	var ARCHIVED = "ARCHIVED";
	var PENDINGCOMPLETIONAPPROVAL = "PENDINGCOMPLETIONAPPROVAL";
	var DECLINED = "DECLINED";
	var COMPLETED = "COMPLETED";
	var ACTIVE = "ACTIVE";

	/**
	 * Gets all the challenge requests made to a user
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getAllChallengeRequests = function(userId) {
        var ref = FDBRoot.child("user").child(userId).child("challenge_requests");
		return $firebaseArray(ref);
    };
	
	retObj.getChallengePreview = function(challengeRequestId) {
		var result = null;
		var ref = FDBRoot.child("challengeRequest").child(challengeRequestId).child("challenge");
		ref.on("value", function(snapshot){
			var challengeRequest = snapshot.val();
			
				var tempChallengeRequest = {
						bounty : "",
						challengeRequestId: "",
						challengeeUsername: "",
						challengeeName: "",
						challengeeImage: "",
						challengeeId: "",
						deadline: "",
						description: "",
						challengerName: "",
						challengerUsername: "",
						challengerImage: "",
						challengerId:"",
						image: "",
						category: "",
						charityId: "",
						charityName: "",
						charityPercentage: "",
						charityImage:"",
						dateSent: ""
					}

					tempChallengeRequest.deadline = challengeRequest.deadline
					FDBRoot.child("challengeRequest").child(challengeRequestId).on("value", function(snapshot2){
						
						var date = genericService.getTime(snapshot2.val().created)
						tempChallengeRequest.dateSent = date;
						tempChallengeRequest.challengeRequestId = challengeRequestId
					});
					
					FDBRoot.child("user").child(challengeRequest.challengee).on("value", function(snapshot3){
						
											tempChallengeRequest.challengeeName = snapshot3.val().name
											tempChallengeRequest.challengeeUsername = snapshot3.val().username
											tempChallengeRequest.challengeeImage = snapshot3.val().profileImageURL
									}) 
					FDBRoot.child("user").child(challengeRequest.challenger).on("value", function(snapshot4){

							tempChallengeRequest.challengerName = snapshot4.val().name
							tempChallengeRequest.challengerUsername = snapshot4.val().username
							tempChallengeRequest.challengerImage = snapshot4.val().profileImageURL
								})   

					FDBRoot.child("charity").child(challengeRequest.charity).on("value", function(snapshot5){
							tempChallengeRequest.charityImage = snapshot5.val().profileImage
							tempChallengeRequest.charityName = snapshot5.val().name
								}) 
								
					tempChallengeRequest.created = challengeRequest.created
					tempChallengeRequest.charityId = challengeRequest.charity
					tempChallengeRequest.charityPercentage = challengeRequest.charityPercentage
					tempChallengeRequest.challengerId = challengeRequest.challenger;
					tempChallengeRequest.challengeeId = challengeRequest.challengee;
					tempChallengeRequest.name = challengeRequest.name;
					tempChallengeRequest.description = challengeRequest.description;
					tempChallengeRequest.bounty = challengeRequest.bounty;
					tempChallengeRequest.category = challengeRequest.category;
					tempChallengeRequest.challengeImage = challengeRequest.image;
					result = tempChallengeRequest
						
		})
		return result;
	}
    /**
	 * Gets all open challenge Requests
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getAllOpenChallengeRequests = function(userId) {
        var result = [];
		var ref = FDBRoot.child("challengeRequest-by-user-receiver").child(userId);	
		ref.on("value", function(snapshot){
			var challengeRequests = snapshot.val();
			angular.forEach(challengeRequests, function(value, key){
				var challengeRequestRef = FDBRoot.child("challengeRequest").child(key);
				challengeRequestRef.on("value", function(snapshot2){
					var challengeRequest = snapshot2.val();
					if(challengeRequest.status == PENDING){
						var tempChallengeRequest = {
                                created : "",
                                id: "",
                                receiverUsername: "",
                                senderUsername: "",
                                receiverId: "",
                                senderId: "",
                                senderImage: "",
                                status: "",
                                name: "",
								challengeImage: ""
                            }

                            tempChallengeRequest.created = challengeRequest.created
                            tempChallengeRequest.id = challengeRequest.id
                            FDBRoot.child("user").child(challengeRequest.receiver).on("value", function(snapshot){
                                                    tempChallengeRequest.receiverUsername = snapshot.val().username
                                            }) 
                            FDBRoot.child("user").child(challengeRequest.sender).on("value", function(snapshot){
                                    tempChallengeRequest.name = snapshot.val().name
                                    tempChallengeRequest.senderUsername = snapshot.val().username
                                    tempChallengeRequest.senderImage = snapshot.val().profileImageURL
                                        })                            
                            tempChallengeRequest.status = challengeRequest.status
                            tempChallengeRequest.receiverId = challengeRequest.receiver;
                            tempChallengeRequest.senderId = challengeRequest.sender;
							tempChallengeRequest.challengeImage = challengeRequest.challenge.image;
						    result.push(tempChallengeRequest);
					}	
				})
			})	
		})
		return result;
    };

	/**
	 * Gets a challenge request by Id
	 * @param userId
	 * @param challengeRequestId
	 * @returns {XMLList}
     */
	retObj.getChallengeRequestById = function(userId, challengeRequestId) {
		return userRef.child(userId).child("challenge_requests").child(challengeRequestId);
	},

	/**
	 * Creates a new challengeRequestService
	 * @param userId
	 * @param userId2
	 * @returns {*}
     */
	retObj.createChallengeRequest = function(senderId, receiverId, challengeParams) {
		//if(this.isDuplicate(senderId, receiverId)) { return false; }
		var challengeRequestRef = FDBRoot.child("challengeRequest");
		var imageRef = FDBRoot.child("challengeCategory").child(challengeParams.category)
		imageRef.on("value", function(snapshot){
				challengeParams.image = snapshot.val().image;
			})
		
		
		var newChallengeRequest = challengeRequestRef.push();
		newChallengeRequest.set({
			id: newChallengeRequest.key(),
			sender: senderId,
			receiver: receiverId,
			created: Firebase.ServerValue.TIMESTAMP,
			challenge: challengeParams,
			status: PENDING,
			accepted: null
		});

		var challengeRequestId = newChallengeRequest.key();
		var challengeRequestByUserSenderRef = FDBRoot.child("challengeRequest-by-user-sender").child(senderId).child(challengeRequestId);
		var challengeRequestByUserReceiverRef = FDBRoot.child("challengeRequest-by-user-receiver").child(receiverId).child(challengeRequestId);
		var challengeRequests = {}
		
		challengeRequestByUserSenderRef.set({
			id : challengeRequestId
		})

		challengeRequestByUserReceiverRef.set({
			id : challengeRequestId
		})

		/**
		 * STARTNOTIF
		 */
			
			
				FDBRoot.child("user").child(senderId).on("value", function(snapshot3){
					var target = []
					target[0] = receiverId
					var user = snapshot3.val();
					var message = user.name + " has challenged you"
					var type = "Challenge Request"
					var owner = senderId
					var uisref = ""
					notificationService.createNotificationChallengeRequest(challengeRequestId,owner, type, message, target, uisref )
				});
			
		/**
		 * END NOTIF
		 */

		return challengeRequestId;
	},

	/**
	 *
	 * @param userId
	 * @param userId2
	 * @returns {boolean}
     */
	retObj.isDuplicate = function(senderId, receiverId) {
		//Search through list of user challenge requests
		//if the logged user has sent the request to a new challenge, a new one cant be created
		//unless the status reads removed.
        var ref = FDBRoot.child("user").child(receiverId).child("challenge_requests");
        var challengeRequests = $firebaseArray(ref);
        challengeRequests.$loaded().then(function (){
            for(var i = 0; i < challengeRequests.length; i++) {
                if(challengeRequests[i].receiver == receiverId && challengeRequests[i].sender == senderId ) {
                    return true;
                }
            }
            return false;
        })

	};

    retObj.acceptChallengeRequest = function(userId, challengeRequestId) {		
		var result
        var ref = FDBRoot.child("challengeRequest").child(challengeRequestId);
		ref.on("value", function(snapshot){
            var challengeRequest = snapshot.val();	
			if(challengeRequest.sender == challengeRequest.receiver){
				var followerRefChallenger = FDBRoot.child("follower-by-challenge").child(challengeRequest.id).child(challengeRequest.sender);
					followerRefChallenger.set({
						id: challengeRequest.sender
					})
			} else{
					var followerRefChallenger = FDBRoot.child("follower-by-challenge")
											.child(challengeRequest.id).child(challengeRequest.sender);
					followerRefChallenger.set({
						id: challengeRequest.sender
					})

					var followerRefChallengee = FDBRoot.child("follower-by-challenge")
											.child(challengeRequest.id).child(challengeRequest.receiver);
					followerRefChallengee.set({
						id: challengeRequest.receiver
					})
				}	

			ref.set({
				id: challengeRequest.id,
				sender: challengeRequest.sender,
				receiver: challengeRequest.receiver,
				created: challengeRequest.created,
				challenge: challengeRequest.challenge,
				status: ACTIVE,
				accepted: true
			})
			challengeService.createChallenge(challengeRequest)
		})
	}
	
    retObj.declineChallengeRequest = function(challengeRequestId) {
		var result
        var ref = FDBRoot.child("challengeRequest").child(challengeRequestId);
		ref.on("value", function(snapshot){
            var challengeRequest = snapshot.val();	
			ref.set({
				id: challengeRequest.id,
				sender: challengeRequest.senderId,
				receiver: challengeRequest.receiverId,
				created: challengeRequest.created,
				challenge: challengeRequest.challenge,
				status: DECLINED,
				accepted: false,
			})
		})
    }

	return retObj;
}])
