    'use strict';
	/**
	 * Contains all functionality for charities
	 * @author      Martin Ombura <martin.omburajr@gmail.com>
	 * @copyright   Copyright (c) Creative Sense.
	 */


angular.module('challengeCategoryService', ['firebase'])
	.factory('challengeCategoryService', ['FDBRoot', '$firebaseArray', '$firebaseObject', function (FDBRoot, $firebaseArray, $firebaseObject) {
		var userRef = FDBRoot.child("users");
		var challengeCategoryRef = FDBRoot.child('challengeCategory');
		return {

			/**
			 * Obtains a charity based on their Id
			 */
			getChallengeCategoryById: function(challengeCategoryId) {
				var ref = FDBRoot.child('challengeCategory').child((challengeCategoryId));
				return $firebaseObject(ref);
			},

			getChallengeCategoryByName: function(name){
				var result = null
				var ref = FDBRoot.child("challengeCategory-by-name").child(name);	
				ref.on("value", function(snapshot){
					var challengeCategoryId = snapshot.val();
					var challengeCategoryRef = FDBRoot.child("challengeCategory").child(challengeCategoryId);
					challengeCategoryRef.on("value", function(snapshot2){
							result = snapshot2.val();
						})
				})
				return result;
			},
		
			/**
			 * Obtains all the charities
			 */
			getAllChallengeCategories: function () {
				return $firebaseArray(challengeCategoryRef);
			}
		}
	}]);