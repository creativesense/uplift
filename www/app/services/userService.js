'use strict';

/**
 * User Service whose purpose
 * Houses the logic and interactions with the user module.
 *
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>
 * 
 * @copyright   Copyright (c) Creative Sense.
 */

angular.module('user', [])

/**
 * Userservice-Username Service - Handles all usernames
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
.factory('userService',['FDBRoot', 'Users', '$firebaseArray', '$firebaseObject', '$state', '$rootScope', function(FDBRoot, Users, $firebaseArray, $firebaseObject, $state, $rootScope){

    var usernameRef = FDBRoot.child("user-by-username");

    var username = {};

    username.getAllUsers = function(){
        var result = [];
        var ref = FDBRoot.child("user");
        return $firebaseArray(ref);
    }

    username.getUserById = function(userId){
        var result = null;
        var ref = FDBRoot.child("user").child(userId);
        ref.on("value", function(snapshot){
            result = snapshot.val();
        })
        return result;
    }

    username.createUsername = function(userId, username){
        var ref = FDBRoot.child("user-by-username").child(username);
        ref.on("value", function(snapshot){

            var obj = snapshot.val();
            if(obj == null) {
                 var updateRefUser = FDBRoot.child("user").child(userId);
                 updateRefUser.on("value", function(snapshot){
                var user = snapshot.val();
                    updateRefUser.set({
                        bio: user.bio,
                        created: user.created,
                        email: user.email,
                        id: user.id,
                        name: user.name,
                        profileImageURL: user.profileImageURL,
                        updated: user.updated,
                        username: username,
                        completedChallenges: user.completedChallenges,
                        ongoingChallenges: user.ongoingChallenges,
                        failedChallenges: user.failedChallenges,
                        successfulChallenges: user.successfulChallenges,
                        bounty: user.bounty                       
                    });
                 })
                    
                 var updateRefRoot = FDBRoot.child("user-by-username").child(username).child(userId);
                    updateRefRoot.set({
                        id: userId
                    });

                var title = "Welcome"
                var message = "Welcome "+username+" to Uplift"
                $rootScope.alert(title, message) 
                $state.go("app.profile")
            } 
            // else {
            //         var title = "Error"
            //         var message = "The username is not available"
            //         $rootScope.alert(title, message)
            //     }
        })
    }

    username.getUserByUsernameSynchronous = function(){

    }

    username.getUsernameById = function(userId){
        var result = null
        var ref = FDBRoot.child("user").child(userId);
        ref.on("value", function(snapshot){
            result = snapshot.val().username;
        })   
        return result
    }

    username.getUserByUsername = function(username){
        var result = null
        var ref = FDBRoot.child("user-by-username").child(username);
        ref.on("value", function(snapshot){
            var userId = snapshot.val();
            var userRef = FDBRoot.child("user").child(userId);
            userRef.on("value", function(snapshot2) {
                result = snapshot2.val()
            })     
        })   
        return result
    }

	return username;
}])

.factory('User', function(Users, challengeService, $rootScope) {
    var User = function() {
        var that = {};

        that._init = function(){
            that._authData = null;
            //that._metaData = null;
        };

        that._init();

        that.SetUserAuthData = function(authData, callbackFinish){
            that._authData = authData;
            that._insertUserDataIfUserDontExists(authData, callbackFinish);
            //that._getMetadata();
        };

        that.Reset = function(){
            that._init();
        };

        that.GetAuthData = function() {
            return that._authData;
        };

        that.GetDisplayName = function() {
            return that._getNameFromAuthData(that._authData);
        };

        that.GetProfileImageURL = function() {
            return that._authData[that._authData.provider].profileImageURL;
        };

        that.GetEmail = function() {
            return that._authData[that._authData.provider].email;
        };

        that.GetCompletedChallenges = function() {
            return that._authData[that._authData.provider].completedChallenges;
        };

        that.GetFailedChallenges = function() {
            return that._authData[that._authData.provider].failedChallenges;
        };

        that.GetOngoingChallenges = function() {
            return that._authData[that._authData.provider].ongoingChallenges;
        };
        that.GetBounty = function() {
            return that._authData[that._authData.provider].bounty;
        };
        // that.GetMetadata = function() {
        //     return that._metaData;
        // };

        // that._getMetadata = function() {
        //     if(that._authData) {
        //         Users.getUserMetadata(that._authData.uid).once('value', function (metaDataSnapshot) {
        //             that._metaData = metaDataSnapshot.val();
        //         }, function (err) {
        //             $rootScope.clog(err);
        //         });
        //     } else {
        //         return {profileImageURL: ''};
        //     }
        // };

        that._insertUserDataIfUserDontExists = function(authData, callbackFinish) {
            var outStatus = {ok: false, msg: 'Unknown error.'};

            Users.getUserById(authData.uid)
            .transaction(
                function(currentData) {    
                    //write initial data only if data dont exists
                    if (currentData === null && authData && authData.hasOwnProperty("provider") && authData[authData.provider].hasOwnProperty("email") && authData[authData.provider].email) {
                        var id = $rootScope.User.GetAuthData().uid;
                        return {
                            id: authData.uid,
                            provider: authData.provider,
                            email: authData[authData.provider].email,
                            username: "",
                            name: that._getNameFromAuthData(authData),
                            profileImageURL: authData[authData.provider].profileImageURL || '',
                            created: Firebase.ServerValue.TIMESTAMP,
                            updated: Firebase.ServerValue.TIMESTAMP,
                            completedChallenges: 0,
                            ongoingChallenges: 0,
                            failedChallenges: 0,
                            successfulChallenges: 0,
                            bounty:0
                        };
                    } else {
                         // Abort the transaction if user data exists.
                    }
                },
                function(error, committed, snapshot) {
                    if (error) {
                        outStatus.msg = 'Transaction failed abnormally! [' + error + ']';
                        callbackFinish && callbackFinish(outStatus);
                    } else if (!committed) {
                        if(!authData || !authData.hasOwnProperty("provider") || !authData[authData.provider].hasOwnProperty("email")) {
                            outStatus.msg = 'Wrong user data!';
                            callbackFinish && callbackFinish(outStatus);
                        } else {
                            outStatus.ok = true;
                            outStatus.msg = '';
                            that._checkInvites(authData.uid, authData[authData.provider].email)
                            .then(function() {
                                that._cleanUserChallengeMembership(authData.uid);
                                callbackFinish && callbackFinish(outStatus);
                            },
                            function(error) {
                                // Something went wrong.
                                console.error(error);
                                callbackFinish && callbackFinish(outStatus);
                            });
                        }
                    } else {

                        Users.getUserIdByEmail(Users.emailToKey(authData[authData.provider].email)).update({
                            user_id: authData.uid
                        }, function(error) {
                            if (error) {
                                outStatus.msg = 'Error:' + error;
                                callbackFinish && callbackFinish(outStatus);
                            } else {
                                var userMetaRef = new Firebase('https://upliftsa.firebaseio.com/user/'+authData.uid);
                                userMetaRef.set({
                                    id: authData.uid,
                                    email: authData[authData.provider].email,
                                    name: that._getNameFromAuthData(authData),
                                    bio: "",
                                    completedChallenges: 0,
                                    ongoingChallenges: 0,
                                    failedChallenges: 0,
                                    successfulChallenges: 0,
                                    bounty: 0,
                                    profileImageURL: authData[authData.provider].profileImageURL || '',
                                    created: Firebase.ServerValue.TIMESTAMP,
                                    updated: Firebase.ServerValue.TIMESTAMP
                                }, function(error) {
                                    if (error) {
                                        outStatus.msg = 'Error:' + error;
                                    } else {
                                        //that._getMetadata();
                                        outStatus.ok = true;
                                        outStatus.msg = '';
                                    }
                                    that._checkInvites(authData.uid, authData[authData.provider].email)
                                    .then(function() {
                                        that._cleanUserChallengeMembership(authData.uid);
                                        callbackFinish && callbackFinish(outStatus);
                                    },
                                    function(error) {
                                        // Something went wrong.
                                        console.error(error);
                                        callbackFinish && callbackFinish(outStatus);
                                    });
                                });
                            }
                        });

                    }
                }
            );
        };

        that._checkInvites = function(userId, userEmail) {
            return Users.getUserIdByEmail(Users.emailToKey(userEmail))
                .once("value")
                .then(function(dataSnapshot) {
                    if(dataSnapshot && dataSnapshot.val().hasOwnProperty("invites")) {
                        return dataSnapshot;
                    } else {
                        return null;
                    }
                })
                .then(function(invitesSnapshot) {
                    if(invitesSnapshot) {
                        for(var challengeId in invitesSnapshot.val().invites) {
                            challengeService.getChallengeById(challengeId)
                                .once("value")
                                .then(function(dataSnapshot) {
                                    var snapVal = dataSnapshot.val();
                                    if(snapVal) {
                                        if(snapVal.hasOwnProperty("members") && snapVal.members.hasOwnProperty(invitesSnapshot.val().user_id)) {
                                            $rootScope.clog("User existed in challenge!");
                                        } else {
                                            challengeService.addFollower(invitesSnapshot.val().user_id, challengeId);
                                        }
                                        invitesSnapshot.ref().child("invites").child(challengeId).remove();


                                    }
                                });
                        }
                    }
                });
        };

        that._cleanUserChallengeMembership = function(userId) {
            console.log(userId);
            if(!userId) return;

            var userChallengesRef = Users.getUserById(userId).child("challenges");

            userChallengesRef.once("value")
                .then(function(userChallengesSnapshot) {
                    var challengesMemberPromises = [];
                    if(userChallengesSnapshot && userChallengesSnapshot.val()) {
                        userChallengesSnapshot.forEach(function (userChallengeSnapshot) {
                            challengesMemberPromises.push(
                                challengeService.getChallengeById(userChallengeSnapshot.key())
                                    .child("members")
                                    .child(userId)
                                    .once("value")
                            );
                        });
                        return challengesMemberPromises;
                    } else {
                        return null;
                    }
                })
                .then(function (challengesMemberPromises) {
                    if(challengesMemberPromises) {
                        return Promise.all(challengesMemberPromises);
                    } else {
                        return null;
                    }
                })
                .then(function (challengesMember) {
                    // if(!challengesMember) return null;

                    // challengesMember.forEach(function (challengeUserSnapshot) {
                    //     if(challengeUserSnapshot.val() === null || challengeUserSnapshot.val() === false) {
                    //         var challengeId = challengeUserSnapshot.ref().path.u[1];
                    //         userChallengesRef.child(challengeId).remove(function(err) {
                    //             if(err) {
                    //                 console.error("_cleanUserChallengeMembership", err);
                    //             } else {
                    //                 console.log("Challenge " + challengeId + " removed from user challenges.");
                    //             }
                    //         });
                    //     }
                    // });
                },
                function(err) {
                    if(err) {
                        console.error("_cleanUserChallengeMembership", err);
                    }
                })
        };

        that._getNameFromAuthData = function(authData) {
            if(authData) {
                switch(authData.provider) {
                    case 'password':
                        return authData.password.email.replace(/@.*/, '');
                    default:
                        return authData[authData.provider].displayName;
                }
            } else {
                throw new Error("_getNameFromAuthData(): Argument authData is not set!");
            }
        };

        return that;
    };

    return {
        get: function() {
            return User();
        }
    };
});