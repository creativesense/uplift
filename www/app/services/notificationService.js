    'use strict';
	/**
	 * Contains all functionality for notification
	 * @author      Martin Ombura <martin.omburajr@gmail.com>
	 * @copyright   Copyright (c) Creative Sense.
	 */


angular.module('notificationService', ['firebase'])
	.factory('notificationService', ['FDBRoot', '$firebaseArray', '$firebaseObject', function (FDBRoot, $firebaseArray, $firebaseObject) {
		var notificationRef = FDBRoot.child('notification');
        var notificationChallengeRef = FDBRoot.child('notification-by-challenge');
        var notificationFriendRequestRef = FDBRoot.child('notification-by-friendRequest');
        var notificationChallengeRequestRef = FDBRoot.child('notification-by-challengeRequest');
        var notificationChallengeProgressRef = FDBRoot.child('notification-by-challengeProgress');
        var notificationChallengeResponseRef = FDBRoot.child('notification-by-challengeResponse');
        var notificationChallengeFollowerRef = FDBRoot.child('notification-by-challengeFollower');
        var notificationChallengeCommentRef = FDBRoot.child('notification-by-challengeComment');
        var notificationChallengeMediaRef = FDBRoot.child('notification-by-challengeMedia');
        var notificationUserRef = FDBRoot.child('notification-by-user');
        
        var retObj = {};

			/**
			 * Obtains a notification based on their Id
			 */
			retObj.getNotificationById = function(notificationId) {
                var tempNotification = {}
				var ref = FDBRoot.child('notification').child(notificationId);
				ref.on("value", function(snapshot){
                    var notification = snapshot.val();
                    tempNotification.title = notification.type;                  
                    var userRef = FDBRoot.child("user").child(notification.owner).on("value", function(snapshot2){
                        tempNotification.owner = snapshot2.val();
                    })
                    tempNotification.message = notification.message;
                    tempNotification.type = notification.type;
                    tempNotification.created = notification.created;
                    tempNotification.typeId = notification.typeId;
                    tempNotification.uisref = notification.uisref
                })
                return tempNotification;
			}

            retObj.getNotificationByUser = function(userId){
                var result = []
                notificationUserRef.child(userId).on("value", function(snapshot){
                    var userNotifications = snapshot.val();
                    angular.forEach(userNotifications, function(value, key){
                        var notifications = retObj.getNotificationById(key);
                        result.push(notifications);
                    })
                })
                return result;
            }
		
			/**
			 * Creates an notification
			 */           
            retObj.createNotificationChallengeRequest = function(challengeRequestId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key()
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeRequestId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeRequestRef.child(challengeRequestId).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

            /**
			 * Creates an notification
			 */           
            retObj.createNotificationFriendRequest = function(friendRequestId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : friendRequestId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationFriendRequestRef.child(friendRequestId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

            /**
			 * Creates an notification
			 */           
            retObj.createNotificationChallengeMedia = function(challengeId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeMediaRef.child(challengeId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

            /**
			 * Creates an notification
			 */           
            retObj.createNotificationChallengeComment = function(challengeId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeCommentRef.child(challengeId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

                        /**
			 * Creates an notification
			 */           
            retObj.createNotificationChallengeFollower = function(challengeId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeCommentId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeCommentRef.child(challengeCommentId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

            /**
			 * Creates an notification
			 */           
            retObj.createNotificationChallengeResponse = function(challengeRequestId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeRequestId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeResponseRef.child(challengeRequestId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

            /**
			 * Creates an notification
			 */           
            retObj.createNotificationChallengeProgress = function(challengeId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeProgressRef.child(challengeId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

            retObj.createNotificationChallenge = function(challengeId, userId, type, messageParams, target, uisref){
                var newNotif = notificationRef.push();
                var id = newNotif.key();
                newNotif.set({
                    id : newNotif.key(),
                    owner : userId,
                    type : type,
                    typeId : challengeId,
                    message : messageParams,
                    created : Firebase.ServerValue.TIMESTAMP,
                    uisref : uisref,
                    target : target
                })
                notificationChallengeRef.child(challengeId).child(id).set({
                    id : id
                })
                retObj.sendToTarget(target, id);
            }

           retObj.getNotificationByChallenge = function(challengeId){
                var result = []
                var tempNotification = {}
                var notificationChallengeRef = FDBRoot.child("notification-by-challenge").child(challengeId)
                    notificationChallengeRef.on("value", function(snapshot){
                        var notificationChallenges = snapshot.val();
                        angular.forEach(notificationChallenges, function(value, key){
                            var tempResult = retObj.getNotificationById(key)
                            result.push(tempResult);
                        })
                    })
                return result;
            }

            retObj.sendToTarget = function(target, notificationId){
                angular.forEach(target, function(value, key){
                    var ref = FDBRoot.child("notification-by-user").child(value).child(notificationId);
                    ref.set({
                        id:notificationId
                    });
                })
            }
		
        return retObj;
	}])