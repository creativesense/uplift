    'use strict';
	/**
	 * Contains all functionality for vendors
	 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>
	 * @copyright   Copyright (c) Creative Sense.
	 */

angular.module('vendorService', ['firebase'])
	.factory('vendorService', ['FDBRoot', '$firebaseArray', '$firebaseObject', function (FDBRoot, $firebaseArray, $firebaseObject) {
		var vendorRef = FDBRoot.child('vendor');
		return {
			/**
			 * Obtains a vendor based on their Id
			 */
			getVendorById: function(vendorId) {
				var ref = FDBRoot.child('vendor').child(vendorId);
				return $firebaseObject(ref);
			},
		
			/**
			 * Obtains all the vendors
			 */
			getAllVendors: function () {
				return $firebaseArray(vendorRef);
			}
		}
	}]);