'use strict';

angular.module('attachmentService', ['firebase'])

/**
 *
 */
.factory('attachmentService', ['FDBRoot', 'Users', 'challengeService', function(FDBRoot, Users, challengeService) {
	var attachmentsRef = FDBRoot.child("attachments");
	var challengessRef = FDBRoot.child("challenges");
	var userMetadata = FDBRoot.child("user-metadata");
	var currentAttachmentId = null;

	var retObj = {};

	retObj.All = function() {
		return attachmentsRef;
	};

	retObj.Attachment = function(attachmentId) {
		return attachmentsRef.child(attachmentId);
	};

	retObj.AttachmentAdd = function(userId, attachmentName) {
		if (!userId || !attachmentName) return;

		var addedAttachment = attachmentsRef.push();

		if(!addedAttachment) return;

		var form = {
            id: addedChallenge.key(),
            owner: userId,
            name: attachmentName,
            isArchived: false,
            created: Firebase.ServerValue.TIMESTAMP,
            updated: Firebase.ServerValue.TIMESTAMP,
            followers: {}
        };
        form.followers[userId] = true;

        //set challenge value
        addedAttachment.set(form, function(err) {
        	if(err) {
        		console.error(err);
        	} else {
				//add challenge to user
    			Users.User(userId).child("attachments").child(addedAttachment.key()).set(true);
        	}
        });
	};

	retObj.setCurrentAttachmentId = function(attachmentId){
		currentAttachmentId = attachmentId;
	};

	retObj.getCurrentAttachmentId = function(){
		return currentAttachmentId;
	};

	return retObj;
}])