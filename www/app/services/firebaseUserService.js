'use strict';

angular.module('firebaseUserService', ['firebase'])


/**
 * Comment Service - Handles all followers
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
.factory('firebaseUserService',['FDBRoot', 'Users', '$firebaseArray', '$firebaseObject', function(FDBRoot, Users, $firebaseArray, $firebaseObject){
	var retObj = {};

    retObj.getUserById = function(userId) {
        var ref = FDBRoot.child("users").child(userId);
        return $firebaseObject(ref);
    }

    retObj.getUserByUsername = function(username) {
        var ref = FDBRoot.child("users").child(userId);
        return $firebaseObject(ref);
    }

    retObj.editUser = function(userId, userParams) {

    }

    retObj.deleteUser = function(userId) {
        
    }
   
	return retObj;
}])
