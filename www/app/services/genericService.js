'use strict';

angular.module('genericService', ['firebase'])

/**
 * Contains all general functionality
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
	.factory('genericService', ['FDBRoot', function (FDBRoot) {

		var retObj = {};

		/**
		 * Converts Firebase TimeStamp to a normal string
		 */
		retObj.getTime = function (UNIX_timestamp) {
			var a = new Date(UNIX_timestamp * 1000);
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			var year = a.getFullYear();
			var month = months[a.getMonth()];
			var date = a.getDate();
			var hour = a.getHours();
			var min = a.getMinutes();
			//var sec = a.getSeconds();
			var time = date + ' ' + month + ' ' + year + ' | ' + hour + ':' + min;
			return time;
		};


	return retObj;
}]);