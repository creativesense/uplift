'use strict';

angular.module('followerService', ['firebase'])


/**
 * Follower Service - Handles all followers
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
.factory('followerService',['FDBRoot', 'activityService','notificationService', '$firebaseArray', '$firebaseObject', function(FDBRoot, activityService, notificationService, $firebaseArray, $firebaseObject){
	var challengesRef = FDBRoot.child("challenges");
	var follower = {};

    /**
     * Obtains a follower from the challlenge
     */
    follower.getFollowerByChallenge = function(followerId, challengeId) {
        var result = null
        var ref = FDBRoot.child("follower-by-challenge").child(challengeId).child(followerId);
		ref.on("value", function(snapshot){
			var follower = snapshot.val();
			var followerRef = FDBRoot.child("user").child(follower);
				followerRef.on("value", function(snapshot2){
					result = snapshot2.val();
				})
			})	
		return result;
    }

    follower.exists = function(followerId, challengeId){
         var result = null
        var ref = FDBRoot.child("follower-by-challenge").child(challengeId).child(followerId);
		ref.on("value", function(snapshot){
            var snapshotVal = snapshot.val();
            if(snapshotVal == null || snapshot==undefined){
                return false;
            }else{
                return true;
            }
        })
    }

    /**
     * Obtains all followers in a challenge
     */
    follower.getAllFollowersFromChallenge = function(challengeId){
        var result = []
        var ref = FDBRoot.child("follower-by-challenge").child(challengeId);
        ref.on("value", function(snapshot){
            var followers = snapshot.val();
            angular.forEach(followers, function(value, key){
                var followerRef = FDBRoot.child("user").child(key)
                followerRef.on("value", function(snapshot2){
                    var follower = snapshot2.val();
                    result.push(follower);
                })
            })
        })
        return result;
    }

    /**
     *  adds a follower to challenge
     */
    follower.addFollower = function(followerId, challengeId){
        var result = follower.exists(followerId, challengeId);
        if(result){
        //If does exists
        var response = {title: "Error", message: "Follower Exists"}
        return response
        } else {
            var followerChallengeRef = FDBRoot.child("follower-by-challenge").child(challengeId).child(followerId);
            followerChallengeRef.set({
                id : followerId
            })
            activityService.createActivity(challengeId, followerId, "FOLLOWER", "ADDED")

            var response = {title: "Success", message: "Follower Added"}
            return response
        }
    }

    /**
     * Removes a follower from a challenge
     */
    follower.removeFollowerToChallenge = function(followerId, challengeId){
        var ref = FDBRoot.child("follower-by-challenge").child(challengeId);
        $firebaseObject(ref).$remove(followerId);
        activityService.createActivity(comment.challengeId, comment.owner, "FOLLOWER", "REMOVED")
    }	

	return follower;
}])
