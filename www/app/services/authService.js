'use strict';

/**
 * Authentication Service
 * Houses the logic and interactions with login providers.
 *
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */

angular.module('auth', ['firebase'])

// create a custom Auth factory to handle $firebaseAuth
.factory('Auth', function($firebaseAuth, FDBRoot, $timeout, $state,  $rootScope){
  var auth = $firebaseAuth(FDBRoot);
  return {
    // helper method to login with multiple providers
    loginWithProvider: function loginWithProvider(provider, optionalSettings) {
        return auth.$authWithOAuthPopup(provider, optionalSettings || {});
    },
    // logging in with Facebook
    loginWithFacebook: function login(optionalSettings) {
        return this.loginWithProvider("facebook", optionalSettings || {scope: 'email'});
    },
    // logging in with Google
    loginWithGoogle: function login(optionalSettings) {
        return this.loginWithProvider("google", optionalSettings || {scope: 'email'});
    },
    // logging in with Twitter
    loginWithTwitter: function login(optionalSettings) {
        return this.loginWithProvider("twitter", optionalSettings || {scope: 'include_email'});
    },
    // logging in with Email
    loginWithEmail: function login(userEmail, userPassword) {
      return auth.$authWithPassword({
            email: userEmail,
            password: userPassword
        });
    },
    // wrapping the createUser function //Edited create user function to check for email validity
    createUser: function createUser(userEmail, userPassword) {
       var errMsg ='';
      return auth.$createUser({
            email: userEmail,
            password: userPassword
        }).then(function(user){
                 errMsg = 'Account Created, please log on with your details';
                 alert(errMsg);
            	 $state.go('app.emailLogin');
        }, function(error){
						if (error.code == "EMAIL_TAKEN") {
				     
				             errMsg ="Error:, the chosen email is already registered.";
                        }
                        
                        else if (error.code == "INVALID_EMAIL"){			      
				              errMsg ="Please enter a valid email address";
                        }                  
				       else{
				            errMsg="Sorry, There was an error creating your account. The uplift team will look into the problem";
                        }
                    
                        $rootScope.hideLoading();

                        $rootScope.alert('<b><i class="icon ion-person"></i> Sign Up Error</b>', errMsg);
				});
    },
    
    // passwordRest function
    resetPassword: function(resetEmail){
               var errMsg ='';
				auth.$resetPassword({
					email: resetEmail
				}).then(function(){
					errMsg= 'Password Reset Email was sent successfully. Please log in with new password';
                    $state.go('app.emailLogin');
                    $rootScope.alert('<b><i class="icon ion-person"></i> Password reset </b>', errMsg);
				}).catch(function(error){
					errMsg = "Error";
                     
                      $rootScope.alert('<b><i class="icon ion-person"></i> Password reset </b>', errMsg);
				});            
	},
    
    
    // wrapping the removeUser function
    removeUser: function removeUser(userEmail, userPassword) {
      return auth.$removeUser({
            email: userEmail,
            password: userPassword
        });
    },
    // wrapping the getAuth function
    getAuth: function getAuth() {
        return auth.$getAuth();
    },
    // wrapping the unauth function
    logout: function logout() {
        auth.$unauth();
    },
    // wrap the $onAuth function with $timeout so it processes
    // in the digest loop.
    onAuth: function onLoggedIn(callback) {
      auth.$onAuth(function(authData) {
        $timeout(function() {
            callback(authData);
        });
      });
    }
  };
})
;