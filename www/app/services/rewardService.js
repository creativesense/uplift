'use strict';

angular.module('rewardService', ['firebase'])


/**
 * Comment Service - Handles all rewards
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
.factory('rewardService',['FDBRoot', 'Users', '$firebaseArray', '$firebaseObject', function(FDBRoot, Users, $firebaseArray, $firebaseObject){
	var challengesRef = FDBRoot.child("challenges");
	var reward = {};

    reward.getRewardFromChallenge = function(rewardId, challengeId) {
        var ref = FDBRoot.child("challenges").child(challengeId).child("reward").child(rewardId);
        return $firebaseObject(ref)
    }

    reward.getAllRewardsFromChallenge = function(challengeId){
        var ref = FDBRoot.child("challenges").child(challengeId).child("reward");
        return $firebaseArray(ref)
    }

	return reward;
}])
