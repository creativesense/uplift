'use strict';

/**Challenges Factory
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @author      Marcelo Dauane <marcelo.dauane@gmail.com>
 */
angular.module('challengeService', ['firebase'])

/**
 * CHALLENGE STATES
 * PENDING - This is a challenge that is pending friend request
 * ACTIVE - Ongoing challenge
 * ARCHIVED - this is a challenge that has been stored
 * PENDINGCOMPLETIONAPPROVAL - This challenge is claimed to have been done by the challengee
 * COMPLETE - A completed challenge is one where the challenge is done and ready to be stored.
 * DECLINED - A declined challenge is one hwere the challengee refuses to take on the challenge
 */
.factory('challengeService', ['FDBRoot', 'Users', 'challengeCategoryService', 'userService', 'notificationService', 'genericService', 'followerService', '$firebaseArray', '$firebaseObject',  
						function (FDBRoot, Users, challengeCategoryService, userService, notificationService, genericService, followerService, $firebaseArray, $firebaseObject) {
	var challengesRef = FDBRoot.child("challenges");
	var challengeItemsRef = FDBRoot.child("challenge-items");
	var retObj = {};
	var PENDING = "PENDING";
	var ARCHIVED = "ARCHIVED";
	var PENDINGCOMPLETIONAPPROVAL = "PENDINGCOMPLETIONAPPROVAL";
	var DECLINED = "DECLINED";
	var COMPLETED = "COMPLETED";
	var ACTIVE = "ACTIVE";


	/**Return challenge from specific user
	 *
	 * @param userId
	 * @param archivedChallenges
	 * @returns {IPromise<TResult>|JQueryPromise<void>}
	 */
	
	/**
	 * retrieves Challenge
	 * @param challengeId
	 * @returns {XMLList}
     */
	retObj.getChallengeById = function(challengeId) {

		var result = null;
		var ref = FDBRoot.child("challenge").child(challengeId);
		ref.on("value", function(snapshot2) {

		var tempChallenge = {
			id: "",
			challengeeUsername: "",
			challengeeName: "",
			challengeeImage: "",
			challengeeId: "",
			deadline: "",
			description: "",
			challengerName: "",
			challengerUsername: "",
			challengerImage: "",
			challengerId:"",
			image: "",
			category: "",
			bounty: "",
			charityId: "",
			charityName: "",
			charityPercentage: "",
			charityImage:"",
			isArchived: "",
			completed:"",
			successfulCompletion: "",
			status:"",
			claimedReward : "",
			rewardId: "",
			progress: ""
		}

		var challenge = snapshot2.val()
		tempChallenge.followers = followerService.getAllFollowersFromChallenge(challengeId);
		FDBRoot.child("user").child(challenge.challengee).on("value", function(snapshot3){				
				tempChallenge.challengee = snapshot3.val()
									}) 
		FDBRoot.child("user").child(challenge.challenger).on("value", function(snapshot4){
				tempChallenge.challenger = snapshot4.val()
								})   
		FDBRoot.child("charity").child(challenge.charity).on("value", function(snapshot5){
				tempChallenge.charity = snapshot5.val()
								})
		FDBRoot.child("challengeCategory").child(challenge.category).on("value", function(snapshot6){
				tempChallenge.category = snapshot6.val()
								})

		tempChallenge.status = challenge.status
		if(challenge.status == "COMPLETED"){
			tempChallenge.completion = challenge.completion;
			tempChallenge.successfulCompletion = challenge.successfulCompletion;
			tempChallenge.status = challenge.status;
			tempChallenge.claimedReward = challenge.claimedReward;
			tempChallenge.rewardId = challenge.rewardId;
		}

		
		tempChallenge.deadline = challenge.deadline
		tempChallenge.created = challenge.created
		tempChallenge.isArchived = challenge.isArchived
		tempChallenge.challengeCompleteChallengee = challenge.challengeCompleteChallengee
		tempChallenge.challengeCompleteChallenger = challenge.challengeCompleteChallenger
		tempChallenge.image = challenge.image
		tempChallenge.charityId = challenge.charity
		tempChallenge.charityPercentage = challenge.charityPercentage
		tempChallenge.challengerId = challenge.challenger;
		tempChallenge.challengeeId = challenge.challengee;
		tempChallenge.name = challenge.name;
		tempChallenge.description = challenge.description;
		tempChallenge.bounty = challenge.bounty;
		tempChallenge.id = challenge.id;
		tempChallenge.updated = challenge.updated;
		tempChallenge.progress = challenge.progress;
		result = tempChallenge;
		})   
		return result;
  }


  retObj.getCreatedChallenges = function(userId){
		var result = [];
		var ref = FDBRoot.child("challenge-by-user-challenger").child(userId);
		ref.on("value", function(snapshot){
			var challenges = snapshot.val();
			angular.forEach(challenges, function(value, key){
			var ref2 = FDBRoot.child("challenge").child(key);
				ref2.on("value", function(snapshot2) {

					var tempChallenge = {
						id: "",
						challengeeUsername: "",
						challengeeName: "",
						challengeeImage: "",
						challengeeId: "",
						deadline: "",
						description: "",
						challengerName: "",
						challengerUsername: "",
						challengerImage: "",
						challengerId:"",
						image: "",
						category: "",
						bounty: "",
						charityId: "",
						charityName: "",
						charityPercentage: "",
						charityImage:"",
						isArchived: "",
						followers: {}
					}

					var challenge = snapshot2.val()
					tempChallenge.followers = followerService.getAllFollowersFromChallenge(challenge.id)
					FDBRoot.child("user").child(challenge.challengee).on("value", function(snapshot3){				
							tempChallenge.challengeeName = snapshot3.val().name
							tempChallenge.challengeeUsername = snapshot3.val().username
							tempChallenge.challengeeImage = snapshot3.val().profileImageURL
												}) 
					FDBRoot.child("user").child(challenge.challenger).on("value", function(snapshot4){
							tempChallenge.challengerName = snapshot4.val().name
							tempChallenge.challengerUsername = snapshot4.val().username
							tempChallenge.challengerImage = snapshot4.val().profileImageURL
											})   
					FDBRoot.child("charity").child(challenge.charity).on("value", function(snapshot5){
							tempChallenge.charityImage = snapshot5.val().profileImage
							tempChallenge.charityName = snapshot5.val().name
											}) 

					tempChallenge.deadline = challenge.deadline
					tempChallenge.created = challenge.created
					tempChallenge.isArchived = challenge.isArchived
					tempChallenge.challengeCompleteChallengee = challenge.challengeCompleteChallengee
					tempChallenge.challengeCompleteChallenger = challenge.challengeCompleteChallenger
					tempChallenge.id = challenge.id
					tempChallenge.image = challenge.image
					tempChallenge.charityId = challenge.charity
					tempChallenge.charityPercentage = challenge.charityPercentage
					tempChallenge.challengerId = challenge.challenger;
					tempChallenge.challengeeId = challenge.challengee;
					tempChallenge.name = challenge.name;
					tempChallenge.description = challenge.description;
					tempChallenge.bounty = challenge.bounty;
					tempChallenge.category = challenge.category;
					tempChallenge.updated = challenge.updated;
					tempChallenge.progress = challenge.progress;
					result.push(tempChallenge);
					
					})
				})		
			})
		return result
  }

    retObj.getChallengedChallenges = function(userId){
		var result = [];
		var ref = FDBRoot.child("challenge-by-user-challengee").child(userId);
		ref.on("value", function(snapshot){
			var challenges = snapshot.val();
			angular.forEach(challenges, function(value, key){
			var ref2 = FDBRoot.child("challenge").child(key);
				ref2.on("value", function(snapshot2) {

					var tempChallenge = {
						id: "",
						challengeeUsername: "",
						challengeeName: "",
						challengeeImage: "",
						challengeeId: "",
						deadline: "",
						description: "",
						challengerName: "",
						challengerUsername: "",
						challengerImage: "",
						challengerId:"",
						image: "",
						category: "",
						bounty: "",
						charityId: "",
						charityName: "",
						charityPercentage: "",
						charityImage:"",
						isArchived: ""
					}

					var challenge = snapshot2.val()

					tempChallenge.followers = followerService.getAllFollowersFromChallenge(challenge.id)
					FDBRoot.child("user").child(challenge.challengee).on("value", function(snapshot3){				
							tempChallenge.challengeeName = snapshot3.val().name
							tempChallenge.challengeeUsername = snapshot3.val().username
							tempChallenge.challengeeImage = snapshot3.val().profileImageURL
												}) 
					FDBRoot.child("user").child(challenge.challenger).on("value", function(snapshot4){
							tempChallenge.challengerName = snapshot4.val().name
							tempChallenge.challengerUsername = snapshot4.val().username
							tempChallenge.challengerImage = snapshot4.val().profileImageURL
											})   
					FDBRoot.child("charity").child(challenge.charity).on("value", function(snapshot5){
							tempChallenge.charityImage = snapshot5.val().profileImage
							tempChallenge.charityName = snapshot5.val().name
											}) 

					tempChallenge.deadline = challenge.deadline
					tempChallenge.created = challenge.created
					tempChallenge.isArchived = challenge.isArchived
					tempChallenge.challengeCompleteChallengee = challenge.challengeCompleteChallengee
					tempChallenge.challengeCompleteChallenger = challenge.challengeCompleteChallenger
					tempChallenge.id = challenge.id
					tempChallenge.image = challenge.image
					tempChallenge.charityId = challenge.charity
					tempChallenge.charityPercentage = challenge.charityPercentage
					tempChallenge.challengerId = challenge.challenger;
					tempChallenge.challengeeId = challenge.challengee;
					tempChallenge.name = challenge.name;
					tempChallenge.description = challenge.description;
					tempChallenge.bounty = challenge.bounty;
					tempChallenge.category = challenge.category;
					tempChallenge.progress = challenge.progress;
					tempChallenge.updated = challenge.updated;
					result.push(tempChallenge);
					})
				})		
			})
			
		return result
  }

	/**
	 * Creates a challenge
	 * @param challengeArray
     */
	retObj.createChallenge = function (challengeRequestObject)
	{
		var challengeArray = challengeRequestObject.challenge
		var challengeId = challengeRequestObject.id
		if (!challengeArray.challenger || !challengeArray.challengee || !challengeArray.name) return;
		var challengesRef = FDBRoot.child("challenge")

		//var challengeCategoryId = challengeCategoryService.getChallengeCategoryByName(challengeArray.category);
		var imageRef = FDBRoot.child("challengeCategory-by-name").child(challengeArray.category)
		imageRef.on("value", function(snapshot){

				challengesRef.child(challengeId).set({
					id: challengeId,
					challenger: challengeArray.challenger,
					challengee: challengeArray.challengee,
					name: challengeArray.name,
					category: challengeArray.category,
					status: PENDING,
					description: challengeArray.description,
					bounty: challengeArray.bounty,
					deadline: challengeArray.deadline,
					charity: challengeArray.charity,
					charityPercentage: challengeArray.charityPercentage,
					image: challengeArray.image,
					challengeCompleteChallenger: false,
					challengeCompleteChallengee: false,
					isArchived: false,
					progress: 0,
					created: Firebase.ServerValue.TIMESTAMP,
					updated: Firebase.ServerValue.TIMESTAMP,
					})
			
					if(challengeArray.challenger == challengeArray.challengee){
							var challengeByUserChallengerRef = FDBRoot.child("challenge-by-user-challenger").child(challengeArray.challenger).child(challengeId);
							challengeByUserChallengerRef.set({
							id : challengeId
							})	
						console.log("Created Succesfully");
						
						/**
						 * STARTNOTIF
						 */
							FDBRoot.child("user").child(challengeArray.challengee).on("value", function(snapshot3){
								var target = []
								target[0] = challengeArray.challenger
								var user = snapshot3.val();
								var message = user.name + " accepted the " + challengeArray.name + " challenge"
								var type = "Challenge Response"
								var uisref = "app.challenge({id:"+challengeId+"})"
								notificationService.createNotificationChallengeResponse(challengeId,challengeArray.challenger, type, message, target,uisref )
							});
						/**
						 * END NOTIF
						 */
					} else {
							var challengeByUserChallengerRef = FDBRoot.child("challenge-by-user-challenger").child(challengeArray.challenger).child(challengeId);
							challengeByUserChallengerRef.set({
								id : challengeId
							})	

							var challengeByUserChallengeeRef = FDBRoot.child("challenge-by-user-challengee").child(challengeArray.challengee).child(challengeId);
							challengeByUserChallengeeRef.set({
								id : challengeId
							})	
							console.log("Created Succesfully");

						/**
						 * STARTNOTIF
						 */
							FDBRoot.child("user").child(challengeArray.challengee).on("value", function(snapshot3){
								var target = []
								target[0] = challengeArray.challenger
								var user = snapshot3.val();
								var message = user.name + " accepted the " + challengeArray.name + " challenge"
								var type = "Challenge Response"
								var uisref = "app.challenge({id:"+challengeId+"})"
								notificationService.createNotificationChallengeResponse(challengeId,challengeArray.challenger, type, message, target,uisref )
							});
						/**
						 * END NOTIF
						 */
					}

			// })
			})

		return challengeId;
	}

	/**Remove Challenge
	 * @param userId
	 * @param challengeId
	 * @constructor
     */
	retObj.removeChallenge = function(challenge) {
		var ref = FDBRoot.child('challenge').child(challenge.id);
		ref.remove();
	}

	retObj.updateProgress = function(progress, challengeId) {
		var challenge = retObj.getChallengeById(challengeId);
		var followers = followerService.getAllFollowersFromChallenge(challengeId);
		var ref = FDBRoot.child('challenge').child(challengeId);
		ref.update({
			progress : progress
		})

		/**
		 * STARTNOTIF
		 */
			var target = []
			angular.forEach(followers, function(value, key){
				target.push(value.id)
			})
			var message = challenge.challengee.name + " changed the progress to " + progress +"%"
			var type = "Challenge Progress"
			var uisref = "app.challenge({id:"+challengeId+"})"
			var owner = challenge.challenger.id
			notificationService.createNotificationChallengeProgress(challengeId,owner, type, message, target, uisref )
		/**
		 * END NOTIF
		 */
	}
	

	/**
	 * Sets the challenge complete from the challenger
	 * @param challengeId
	 * @param userId
	 */
	retObj.completeChallengeChallenger = function(challengeId) {
		var challenge = retObj.getChallengeById(challengeId);
		var followers = followerService.getAllFollowersFromChallenge(challengeId);
		var ref = FDBRoot.child("challenge").child(challengeId);
		var successful = true;

		retObj.updateChallengeeBounty(challenge, successful);
			
		ref.update({
			status : COMPLETED,
			progress : 100,
			isArchived: true,
			updated : Firebase.ServerValue.TIMESTAMP,
			challengerCompletionConfirmedTime: Firebase.ServerValue.TIMESTAMP,
			successfulCompletion : successful,
			claimedReward : false,
			rewardId : null
		})
		/**
		 * STARTNOTIF
		 */
			var target = []
			angular.forEach(followers, function(value, key){
				target.push(value.id)
			})
			var message = challenge.challenger.name + " has marked the challenge complete. Congratulations!"
			var type = "Challenge Complete"
			var owner = challenge.challenger.id
			var uisref = "app.challenge({id:"+challengeId+"})"
			notificationService.createNotificationChallenge(challengeId,owner, type, message, target, uisref )
		/**
		 * END NOTIF
		 */	
	};

	retObj.updateChallengerBounty = function(challenge){
		// var challengerRef = FDBRoot.child("user").child(challenge.challenger);
		// challengerRef.on("value", function(snapshot){
		// 	var challenger = snapshot.val();
		// 	challengerRef.update({
		// 		bounty: (challenger.bounty + (challenge.bounty)/15)
		// 	})
		// })
	}

	retObj.updateChallengeeBounty = function(challenge, success) {
		var user = null;
		if(challenge.challengee == null || challenge.challengee == undefined){
			user = challenge.challenger;
		}else{
			user = challenge.challengee;
		}
		var challengeeRef = FDBRoot.child("user").child(user.id);
		if(success)
		{
			var temp = 0;
			challengeeRef.on("value", function(snapshot){
				var challengee = snapshot.val();
				var newBounty = challenge.bounty
			challengeeRef.update({
				bounty: newBounty 
				})	
			})
		} else{

		}				 
	}

	/**
	 * Sets the challenge complete from the challengee
	 * @param challengeId
	 * @param userId
	 */
	retObj.completeChallengeChallengee = function(challengeId) {
		var challenge = retObj.getChallengeById(challengeId);
		var ref = FDBRoot.child("challenge").child(challengeId);
		ref.update({              
			status : PENDINGCOMPLETIONAPPROVAL,                       
			updated : Firebase.ServerValue.TIMESTAMP,
			completed :  Firebase.ServerValue.TIMESTAMP
		})

		/**
		 * STARTNOTIF
		 */
			var target = []
			angular.forEach(challenge.followers, function(value, key){
				target.push(value.id)
			})
			var message = challenge.challengee.name + " has claimed to finish the challenge!"
			var type = "Challenge Complete"
			var owner = challenge.challengee.id
			var uisref = "app.challenge({id:"+challengeId+"})"
			notificationService.createNotificationChallenge(challengeId,owner, type, message, target, uisref )
		/**
		 * END NOTIF
		 */	
	};

	retObj.calculateCharityEarnings = function(bounty, charityPercentage){
		return (bounty * (charityPercentage/100));
	}

	retObj.calculateAdminEarnings = function(bounty){
		return (bounty * (5/100));
	}

	retObj.calculateChallengeeEarnings = function(bounty, charityPercentage){
		return bounty - (bounty * (charityPercentage/100)) +(bounty * (5/100)) ;
	}

	retObj.challengeDuration = function(challengeId){
		var ref = FDBRoot.child("challenge").child(challengeId);
		ref.on("value", function(snapshot){
			var challenge = snapshot.val();
			var completionTime = challenge.completed - challenge.deadline;
			return completionTime;
		})
	}

	/**Remove follower from challenge
	 *
	 * @param userId
	 * @param challengeId
	 * @returns {*}
	 * @constructor
	 */
	retObj.challengeRemoveFollower = function(userId, challengeId) {
		if (!userId || !challengeId) return;

		return retObj.Challenge(challengeId)
			.child("followers")
			.child(userId)
			.remove();
	};
	return retObj;
}])

