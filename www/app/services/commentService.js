'use strict';

angular.module('commentService', ['firebase'])


/**
 * Comment Service - Handles all comments
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
.factory('commentService',['FDBRoot', 'Users', 'activityService', 'notificationService', 'challengeService', '$firebaseArray', '$firebaseObject', function(FDBRoot, Users, activityService, notificationService, challengeService, $firebaseArray, $firebaseObject){
	var challengesRef = FDBRoot.child("challenges");
	var comment = {};

	/**
	 * Obtains a comment byId
	 */
	comment.getCommentById = function(commentId){
	  var tempComment = {};
	  
	  var ref = FDBRoot.child("comment").child(commentId);
	  ref.on("value", function(snapshot){
		  var comment = snapshot.val();
		  var userRef = FDBRoot.child("user").child(comment.owner);
		  userRef.on("value", function(snapshot2){
			  var user =snapshot2.val();
			  tempComment.id = comment.id;
			  tempComment.owner = comment.owner
			  tempComment.ownerUsername = user.username;
			  tempComment.ownerImage = user.profileImageURL;
			  tempComment.ownerName = user.name;
			  tempComment.content = comment.content;
			  tempComment.created = comment.created;
			  tempComment.challenge = comment.challenge;
		  })
	  })
	  return tempComment;
	};

	comment.getCommentsByChallengev2 = function(challengeId) {
	  var result = []
	  var tempComment = {}
	  var ref = FDBRoot.child("comment-by-challenge").child(challengeId);
	  ref.on("value", function(snapshot){
		  var challengeComments = snapshot.val();
		  angular.forEach(challengeComments, function(value, key){
			  var comments = comment.getCommentById(key);
			  result.push(comments);
		  })
	  })
	  return result;
  	}

	/**
	 * Gets all the comments associated with a challenge
	 */
	comment.getCommentsByChallenge = function(challengeId) {
		var ref = FDBRoot.child("challenge").child(challengeId).child("comments");
		return $firebaseArray(ref);
	};


	/**
	 * Returns a comment By User
	 */
	comment.getCommentsByUser = function(userId, commentId) {
		var result = []
		var ref = FDBRoot.child("comment-by-user").child(userId).child(commentId);	
		ref.on("value", function(snapshot){
			var comments = snapshot.val();
			angular.forEech(comments, function(value, key){
				var commentRef = FDBRoot.child("comments").child(value);
				commentRef.on("value", function(snapshot2){
					result.push(snapshot2.val());
				})
			})	
		})
		return result;
	};


	/**
	 * Creates a comment
	 * @param userId, challengeId, content
	 */
	comment.createComment = function(comment) {
		var challenge = challengeService.getChallengeById(comment.challengeId);
		var commentsBaseRef = FDBRoot.child("comment");
		var addBaseComment = commentsBaseRef.push();
		addBaseComment.set({
            id: addBaseComment.key(),
            owner: comment.owner,
			challenge: comment.challengeId,
            content: comment.content,
			edited: false,
			editedDate: null,
            created: Firebase.ServerValue.TIMESTAMP,
            updated: Firebase.ServerValue.TIMESTAMP,
        });

		var commentId = addBaseComment.key();
		var commentsByChallengeRef = FDBRoot.child("comment-by-challenge").child(comment.challengeId).child(commentId);
		commentsByChallengeRef.set({
			id : commentId
		})

		var commentsByUserRef = FDBRoot.child("comment-by-user").child(comment.owner).child(commentId);
		commentsByUserRef.set({
			id : commentId
		})

		activityService.createActivity(comment.challengeId, comment.owner, "COMMENT", "CREATED")

		/**
		 * STARTNOTIF
		 */
			
            FDBRoot.child("user").child(comment.owner).on("value", function(snapshot3){
                var target = []
                angular.forEach(challenge.followers, function(value, key){
					target.push(value.id)
				})
                var user = snapshot3.val();
                var message = user.name + " has commented in"
                var type = "Comment"
                var owner = user.id
                var uisref = "app.challenge.comments"
                notificationService.createNotificationChallengeComment(comment.challengeId, owner, type, message, target, uisref)
            });
			
		/**
		 * END NOTIF
		 */

	};

	comment.deleteComment = function(commentId, challengeId) {
		var ref = FDBRoot.child('comment').child(commentId);
		var commentChallengeRef = FDBRoot.child('comment-by-challenge').child(challengeId).child(commentId);
		ref.remove();
		commentChallengeRef.remove();
	};

	comment.editComment = function(commentId, challengeId, newContent) {
		var ref = FDBRoot.child('challenges').child(challengeId).child('comments').child(commentId)
		var tempRef = ref.child();//ref.once();
		ref.update({
			"content": newContent.content,
			"dateTime": Firebase.ServerValue.TIMESTAMP,
			"edited": true,
		});
	};
	return comment;
}])
