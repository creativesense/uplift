
angular.module('friendRequestService', ['firebase'])
/**
 * Facilitates the creation of friendrequests
 * Friend Request Statuses:	PENDING, ACCEPTED, DECLINED, REMOVED
 * @author      Martin Ombura <martin.omburajr@gmail.com>
 * @copyright   Copyright (c) Creative Sense.
 */
	.factory('friendRequestService', ['FDBRoot', 'Users', 'challengeService', 'friendService', 'notificationService', 'genericService', '$firebaseArray', '$firebaseObject', 
                                                    function (FDBRoot, Users, challengeService, friendService, notificationService, genericService, $firebaseArray, $firebaseObject) {
	var userRef = FDBRoot.child("user");
	var retObj = {};
	var PENDING = "PENDING";
	const ACCEPTED = "ACCEPTED";
	const DECLINED = "DECLINED"; 

	/**
	 * Gets all the friend requests made to a user
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getAllFriendRequests = function(userId) {
        var ref = FDBRoot.child("user").child(userId).child("friend_requests");
		return $firebaseArray(ref);
    };

    /**
	 * Gets all open friend Requests
	 * @param userId
	 * @returns {XMLList}
     */
	retObj.getAllOpenFriendRequests = function(userId) {
        var result = [];
		var ref = FDBRoot.child("friendRequest-by-user-receiver").child(userId);	
		ref.on("value", function(snapshot){
            //Gets friendRequests from user
			var friendRequests = snapshot.val();
			angular.forEach(friendRequests, function(value, key){
				var friendRequestRef = FDBRoot.child("friendRequest").child(key);
				friendRequestRef.on("value", function(snapshot2){
                    //Gets the actual friend request
                    var friendRequestObjects = snapshot2.val();
                        if(friendRequestObjects.status == PENDING){

                            var tempFriendRequest = {
                                created : "",
                                id: "",
                                receiverUsername: "",
                                senderUsername: "",
                                receiverId: "",
                                senderId: "",
                                senderImage: "",
                                status: "",
                                name: ""
                            }

                            tempFriendRequest.created = genericService.getTime(friendRequestObjects.created)
                            tempFriendRequest.id = friendRequestObjects.id
                            FDBRoot.child("user").child(friendRequestObjects.receiver).on("value", function(snapshot){
                                                    tempFriendRequest.receiverUsername = snapshot.val().username
                                            }) 
                            FDBRoot.child("user").child(friendRequestObjects.sender).on("value", function(snapshot){
                                    tempFriendRequest.name = snapshot.val().name
                                    tempFriendRequest.senderUsername = snapshot.val().username
                                    tempFriendRequest.senderImage = snapshot.val().profileImageURL
                                        }) 
                                        
                            tempFriendRequest.status = friendRequestObjects.status
                            tempFriendRequest.receiverId = friendRequestObjects.receiver;
                            tempFriendRequest.senderId = friendRequestObjects.sender;
						    result.push(tempFriendRequest);
                            console.log(result);
					    }	
				})
			})	
		})
		return result;
    };


	/**
	 * Creates a new friendRequestService
	 * @param userId
	 * @param userId2
	 * @returns {*}
     */
	retObj.createFriendRequest = function(senderId, receiverId) {
		if(this.isDuplicate(senderId, receiverId)) { return false; }
        if(this.isAlreadyFriend(senderId, receiverId)) {
            return false;}

		var friendRequestRef = FDBRoot.child("friendRequest")
		var newFriendRequest = friendRequestRef.push();
		newFriendRequest.set({
			id: newFriendRequest.key(),
			sender: senderId,
			receiver: receiverId,
			created: Firebase.ServerValue.TIMESTAMP,
			status: "PENDING",
			accepted: null,
		});
        var friendRequestId = newFriendRequest.key();
        var friendRequestByUserSenderRef = FDBRoot.child("friendRequest-by-user-sender").child(senderId).child(friendRequestId).child(receiverId) //Checks for the user that created the friendRequest
        friendRequestByUserSenderRef.set({
            id: senderId
        })
        var friendRequestByUserReceiverRef = FDBRoot.child("friendRequest-by-user-receiver").child(receiverId).child(friendRequestId).child(senderId) //Checks for the user that created the friendRequest
        friendRequestByUserReceiverRef.set({
            id: senderId
        })

        /**
		 * STARTNOTIF
		 */
			
            FDBRoot.child("user").child(senderId).on("value", function(snapshot3){
                var target = []
                target[0] = receiverId
                var user = snapshot3.val();
                var message = user.name + " has added you as a friend"
                var type = "Friend Request"
                var owner = senderId
                var uisref = "app.friendRequests"
                notificationService.createNotificationChallengeRequest(friendRequestId,owner, type, message, target,uisref )
            });
			
		/**
		 * END NOTIF
		 */
	},

    retObj.isAlreadyFriend = function(senderId, receiverId){
        var ref = FDBRoot.child("friend-by-user").child(senderId).child(receiverId);
        ref.on("value", function(snapshot){
            var result = snapshot.val()
            if( result == null || result == undefined){
                return false;
            } else {
                return true;
            }
        })
    }

	/**
	 *
	 * @param userId
	 * @param userId2
	 * @returns {boolean}
     */
	retObj.isDuplicate = function(senderId, receiverId) {
        //Go to "friendRequest-by-user-receiver"
        var friendRequestByUserReceiverRef = FDBRoot.child("friendRequest-by-user-receiver").child(receiverId).child(senderId)
        friendRequestByUserReceiverRef.on("value", function(snapshot){
            var isFriendRequestSent = snapshot.val();
            if(isFriendRequestSent == null){
                return false;
            } else {
                return true;
            }
        })
	};

    retObj.acceptFriendRequest = function(userId, friendRequestId) {
        var result
        var ref = FDBRoot.child("friendRequest").child(friendRequestId);
        //ChangeStatus

        ref.on("value", function(snapshot){
            var friendRequest = snapshot.val();
            console.log(friendRequest)
                //Add sender to receiver
                if(friendRequest.sender == friendRequest.receiver){
                    var senderReceiverRef = FDBRoot.child("friend-by-user").child(friendRequest.sender).child(friendRequest.receiver);
                    senderReceiverRef.set({
                        friendRequestId : friendRequestId
                    })
                } else {
                var senderReceiverRef = FDBRoot.child("friend-by-user").child(friendRequest.sender).child(friendRequest.receiver);
                    senderReceiverRef.set({
                        friendRequestId : friendRequestId
                    })
                var receiverSenderRef = FDBRoot.child("friend-by-user").child(friendRequest.receiver).child(friendRequest.sender);
                    receiverSenderRef.set({
                        friendRequestId : friendRequestId
                    })
                }

                /**
                 * STARTNOTIF
                 */
                    
                    FDBRoot.child("user").child(friendRequest.receiver).on("value", function(snapshot3){
                        var target = []
                        target[0] = friendRequest.sender;
                        var user = snapshot3.val();
                        var message = user.name + " has accepted your friend request"
                        var type = "Friend Request"
                        var owner = friendRequest.receiver
                        var uisref = "app.friends"
                        notificationService.createNotificationFriendRequest(friendRequestId,owner, type, message, target, uisref)
                    });
                    
                /**
                 * END NOTIF
		 */

        })
        ref.update({
            accepted : Firebase.ServerValue.TIMESTAMP,
            status : ACCEPTED
        })

        
    }

    retObj.declineFriendRequest = function(userId, friendRequestId) {
        var result
        var ref = FDBRoot.child("friendRequest").child(friendRequestId);
        ref.on("value", function(snapshot){
        var friendRequest = snapshot.val();
                //ChangeStatus
                ref.set({
                    sender: friendRequest.sender,
                    receiver: friendRequest.receiver,
                    id: friendRequest.id,
                    created: friendRequest.created,
                    accepted : Firebase.ServerValue.TIMESTAMP,
                    status : DECLINED
                })
        })
        
        //ref.remove();
    }

	return retObj;
}])
